package com.mvpmvh.scatterbrained.resources

import com.mvpmvh.scatterbrained.service.GameService
import com.mvpmvh.scatterbrained.service.UserService
import org.apache.log4j.Logger
import org.restlet.Message
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.engine.header.Header
import org.restlet.representation.Representation
import org.restlet.resource.*
import org.restlet.util.Series

import java.util.concurrent.ConcurrentMap

/*** User Controller
 * ----------------
 * username (the player)
 * password (account password)
 * email (account email)
 * buddies (saved player names) * 
 */


class UserResource extends ServerResource {
    def UserService userService
    def GameService gameService

    private static final String HEADERS_KEY = "org.restlet.http.headers"
    private static final Logger logger = Logger.getLogger(UserResource.class)

    static Series<Header> getMessageHeaders(Message message) {
        ConcurrentMap<String, Object> attrs = message.getAttributes();
        Series<Header> headers = (Series<Header>) attrs.get(HEADERS_KEY);
        if (headers == null) {
            headers = new Series<Header>(Header.class);
            Series<Header> prev = (Series<Header>) attrs.putIfAbsent(HEADERS_KEY, headers);
            if (prev != null) {
                headers = prev;
            }
        }
        return headers;
    }

    /**
     * Handle PUT requests.
     *
     * @throws IOException
     */
    @Put
    def putUser(Representation entity) throws IOException {
        def req = getRequestAttributes()
        def userName = req["username"]
        def result
        def form = new Form(entity)
        def _password = form.getFirstValue("password")
        def _email = form.getFirstValue("email")
        def action = form.getFirstValue("action")

        if (action == "addBuddy") {
            def buddy = form.getFirstValue("buddy")
            result = userService.addBuddy(userName, buddy)
        } else if (action == "removeBuddy") {
            def buddy = form.getFirstValue("buddy")
            result = userService.removeBuddy(userName, buddy)
        } else if (action == "updateUser")
            result = userService.updateUser(userName, _email, _password)


        if (result.getJsonObject().has("ERROR"))
            setStatus(Status.CLIENT_ERROR_CONFLICT)
        else
            setStatus(Status.SUCCESS_OK)

        result
    }

    /**
     * Handle POST requests
     *
     * create a new user
     */
    @Post()
    def Representation postUser(Representation entity) {
        def req = getRequestAttributes()
        def form = new Form(entity)
        def userName = form.getFirstValue("username")
        def _password = form.getFirstValue("password")
        def action = req["action"]
        def result

        if (action == "create") {
            def _email = form.getFirstValue("email")
            if (logger.isDebugEnabled()) {
                logger.debug("creating new account: {usernme: ${userName}, email: ${_email}, password: ${_password}")
            }
            result = userService.createUser(userName, _email, _password)
        } else if (action == "login") {
            if (logger.isDebugEnabled()) {
                logger.debug("attempting to login: {username: ${userName}, password: ${_password}")
            }
            result = userService.login(userName, _password)
            //if(result.getJsonObject().has("SUCCESS"))
            //result = gameService.getUserGamesByStatus(userName, "PENDING")
        } else if (action == "reset")
            result = userService.reset(userName)
        else if (action == "forgot") {
            def _email = form.getFirstValue("email")
            result = userService.forgotUsername(_email)
        }

        if (result.getJsonObject().has("ERROR"))
            setStatus(Status.CLIENT_ERROR_CONFLICT)
        else
            setStatus(Status.SUCCESS_OK)

        getMessageHeaders(getResponse()).add("Access-Control-Allow-Origin", "*")
        result
    }

    @Options
    def Representation describe() {
        Form responseHeaders = (Form) getResponse().getAttributes().get("org.restlet.http.headers");
        if (responseHeaders == null) {
            responseHeaders = new Form();
            getResponse().getAttributes().put("org.restlet.http.headers", responseHeaders);
        }
        responseHeaders.add("Access-Control-Allow-Origin", "*");
    }

    /**
     * Returns a listing of all registered users or a specified user.
     */
    @Get()
    def Representation toJson() {
        def req = getRequestAttributes()
        def userName = req["username"]
        userService.getUser(userName)
    }
}