package com.mvpmvh.scatterbrained.resources

import com.mvpmvh.scatterbrained.service.GameService
import com.mvpmvh.scatterbrained.service.UserService
import org.apache.log4j.Logger
import org.restlet.Message
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.engine.header.Header
import org.restlet.representation.Representation
import org.restlet.resource.Get
import org.restlet.resource.Options
import org.restlet.resource.Post
import org.restlet.resource.ServerResource
import org.restlet.util.Series

import java.util.concurrent.ConcurrentMap

class GameResource extends ServerResource {
    def GameService gameService
    def UserService userService

    private static final String HEADERS_KEY = "org.restlet.http.headers"
    private static final Logger logger = Logger.getLogger(GameResource.class)

    static Series<Header> getMessageHeaders(Message message) {
        ConcurrentMap<String, Object> attrs = message.getAttributes();
        Series<Header> headers = (Series<Header>) attrs.get(HEADERS_KEY);
        if (headers == null) {
            headers = new Series<Header>(Header.class);
            Series<Header> prev = (Series<Header>) attrs.putIfAbsent(HEADERS_KEY, headers);
            if (prev != null) {
                headers = prev;
            }
        }
        return headers;
    }

    /**
     * Handle POST requests
     *
     * create a new user
     */
    @Post
    def Representation postGame(Representation entity) {
        def req = getRequestAttributes()
        def form = new Form(entity)
        def gameName = form.getFirstValue("gameName")
        def gameId = req["gameId"]
        def playerName = form.getFirstValue("playerName")
        def action = req["action"]
        def result = userService.userExists(playerName)

        if (result.getJsonObject().has("SUCCESS")) {
            if (action == "create" || action == "update") {
                def numPlayers = Integer.parseInt(form.getFirstValue("numPlayers"))
                def rounds = Integer.parseInt(form.getFirstValue("rounds"))
                def isOpen = (form.getFirstValue("isOpen") == "T" ? true : false)
                //def teamSize = Integer.parseInt(form.getFirstValue("teamSize"))
                def teamSize = 1    //TODO: implement teams
                result = (action == "create") ? gameService.createGame(gameName, playerName, numPlayers, rounds, isOpen, teamSize) :
                    gameService.updateGame(gameName, playerName, numPlayers, rounds, isOpen, teamSize)
            } else if (action == "quit")
                result = gameService.quitGame(gameId, playerName)
            else if (action == "decline")
                result = gameService.declineInvite(gameId, playerName)
            else if (action == "join")
                result = gameService.joinGame(gameId, playerName)
            else if (action == "delete")
                result = gameService.deleteGame(gameId, playerName)
            else if (action == "inviteUser") {
                def invited = form.getFirstValue("invited")
                result = gameService.inviteByUserName(gameId, invited, playerName)
            } else if (action == "inviteEmail") {
                def email = form.getFirstValue("email")
                result = gameService.inviteUserByEmail(gameId, email, playerName)
            } else if (action == "uninvite") {
                def uninvited = form.getFirstValue("uninvited")
                result = gameService.unInviteUser(gameId, uninvited, playerName)
            }
        }

        if (result.getJsonObject().has("ERROR"))
            setStatus(Status.CLIENT_ERROR_CONFLICT)
        else
            setStatus(Status.SUCCESS_OK)

        getMessageHeaders(getResponse()).add("Access-Control-Allow-Origin", "*")
        result
    }

    @Options
    def Representation describe() {
        Form responseHeaders = (Form) getResponse().getAttributes().get("org.restlet.http.headers");
        if (responseHeaders == null) {
            responseHeaders = new Form();
            getResponse().getAttributes().put("org.restlet.http.headers", responseHeaders);
        }
        responseHeaders.add("Access-Control-Allow-Origin", "*");
    }

    @Get
    def Representation toJson() {
        def req = getRequestAttributes()
        def playerName = req["playerName"]
        def status = req["status"]
        getMessageHeaders(getResponse()).add("Access-Control-Allow-Origin", "*")
        if (status == "INVITED") gameService.findInvitedGames(playerName)
        else gameService.getUserGamesByStatus(playerName, status)
    }
}