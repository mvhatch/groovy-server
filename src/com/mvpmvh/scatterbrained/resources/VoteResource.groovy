package com.mvpmvh.scatterbrained.resources

import com.mvpmvh.scatterbrained.service.GameService
import com.mvpmvh.scatterbrained.service.VoteService
import org.restlet.data.Form
import org.restlet.data.Status
import org.restlet.representation.Representation
import org.restlet.resource.Get
import org.restlet.resource.Post
import org.restlet.resource.ServerResource

class VoteResource extends ServerResource {
    def VoteService voteService
    def GameService gameService

    /**
     * Handle POST requests
     *
     * create a new user
     */
    @Post
    def Representation postUser(Representation entity) {
        def form = new Form(entity)
        def gameName = form.getFirstValue("gameName")
        def rd = form.getFirstValue("rd")
        def playerName = form.getFirstValue("playerName")
        def voterName = form.getFirstValue("voterName")
        def finished = form.getFirstValue("finished")
        def votes = []

        (1..10).each {
            votes << (form.getFirstValue("v${it}") == "T") ? true : false
        }
        def result = voteService.submitVotes(gameName, rd, playerName, voterName, votes, finished)
        if (result.getJsonObject().get("SUCCESS") == "Round is completed")
            result = gameService.startNextRound(gameName)

        if (result.getJsonObject().has("ERROR"))
            setStatus(Status.CLIENT_ERROR_CONFLICT)
        else
            setStatus(Status.SUCCESS_OK)

        result
    }

    /**
     * Returns a listing of all registered users or a specified user.
     */
    @Get
    def Representation toJson() {
        def req = getRequestAttributes()
        def gameName = req["gameName"]
        def playerName = req["playerName"]
        def rd = req["rd"]

        if (rd != null) voteService.getVotesForPlayerByRound(gameName, playerName, rd)
        else {
            if (req["byPlayer"] != null) voteService.getVotesByPlayerForGame(gameName, playerName)
            else voteService.getVotesForPlayerForGame(gameName, playerName)
        }
    }
}
