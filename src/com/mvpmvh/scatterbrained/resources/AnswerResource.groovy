package com.mvpmvh.scatterbrained.resources

import com.mvpmvh.scatterbrained.service.AnswerService
import com.mvpmvh.scatterbrained.service.VoteService
import org.restlet.data.Form
import org.restlet.representation.Representation
import org.restlet.resource.Get
import org.restlet.resource.Post
import org.restlet.resource.ServerResource

class AnswerResource extends ServerResource {
    def AnswerService answerService
    def VoteService voteService

    @Post
    def Representation postUser(Representation entity) {
        def form = new Form(entity)
        def gameName = form.getFirstValue("gameName")
        def playerName = form.getFirstValue("playerName")
        def _round = form.getFirstValue("round")
        def finished = form.getFirstValue("finished")
        def answers = []
        def result

        (1..10).each {
            answers << form.getFirstValue("a${it}")
        }

        result = answerService.submitAnswers(gameName, playerName, _round, answers, finished)
        if (result.getJsonObject.get("SUCCESS") == "Answers submitted, ready to vote")
            result = voteService.readyToVote(gameName, playerName, _round)

        if (result.getJsonObject().has("ERROR"))
            setStatus(Status.CLIENT_ERROR_CONFLICT)
        else
            setStatus(Status.SUCCESS_OK)

        result
    }

    @Get
    def Representation toJson() {
        def req = getRequestAttributes()
        def gameName = req["gameName"]
        def playerName = req["playerName"]
        def _round = req["round"]

        playerName == null ? answerService.getUnfinishedAnswersForRound(gameName, _round) :
            answerService.getPlayerAnswersForRound(gameName, playerName, _round)
    }
}
