package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.AnswerDao
import com.mvpmvh.scatterbrained.dao.GameDao
import com.mvpmvh.scatterbrained.dao.UserDao
import com.mvpmvh.scatterbrained.dao.VoteDao
import com.mvpmvh.scatterbrained.domain.Gmail
import org.apache.log4j.Logger
import org.bson.types.ObjectId
import org.restlet.ext.json.JsonRepresentation

class GameService extends BaseService {
    def UserDao userDao
    def GameDao gameDao
    def AnswerDao answerDao
    def VoteDao voteDao
    def Gmail gmail
    def Random random

    private static final Logger logger = Logger.getLogger(GameService.class)

    def createGame(_gameName, _createdBy, _numPlayers, _rounds, _isOpen, _teamSize) {
        def result

        if (_gameName != null) {
            if (logger.isDebugEnabled()) {
                logger.debug("attempting to create game: ${_gameName} now...")
            }
            if (_createdBy != null && _numPlayers > 1 && _rounds > 0 && _teamSize > 0) {
                def _letter = getRandomLetter()
                def _categories = getRandomCategories()
                def game = [gameName: _gameName, createdBy: _createdBy, numPlayers: _numPlayers,
                        rounds: _rounds, isOpen: _isOpen, teamSize: _teamSize, status: "PENDING",
                        pending: [], playing: [_createdBy], letter: _letter, categories: _categories, currentRound: 1]
                gameDao.createGame(game)
                //TODO: handle transaction failures
                result = generateResponseMessage(false, "Game: ${_gameName} successfully created!")
                if (logger.isDebugEnabled()) {
                    logger.debug("game created: ${game}")
                }
            } else {
                result = generateResponseMessage(true, "Invalid game options!")
                if (logger.isDebugEnabled()) {
                    logger.debug("could not create game: ${_gameName} with options: " +
                            "_createdBy: ${_createdBy}, _numPlayers: ${_numPlayers}, _rounds: ${_rounds}, _teamSize: ${_teamSize}")
                }
            }
        } else {
            result = generateResponseMessage(true, "A game without a name is invalid!")
            if (logger.isDebugEnabled()) {
                logger.debug("A game without a name is invalid!")
            }
        }

        result
    }

    def updateGame(_gameId, playerName, _numPlayers, _rounds, _isOpen, _teamSize) {
        def result
        def game = findGameById(_gameId)

        if (game != null && game["createdBy"] == playerName) {
            if (game["status"] == "PENDING") {
                if (_numPlayers > 1 && _rounds > 0 && _teamSize > 0) {
                    gameDao.updateGame(_gameId, _numPlayers, _rounds, _isOpen, _teamSize)
                    result = generateResponseMessage(false, "Game: ${game['gameName']} was successfully modified!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Game: ${game['gameName']} was successfully modified!")
                    }
                } else {
                    result = generateResponseMessage(true, "Invalid udate game options. Number of players: ${_numPlayers};" +
                            " Number of rounds: ${_rounds}; Public: ${_isOpen}; Team size: ${_teamSize}.")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Invalid udate game options. Number of players: ${_numPlayers};" +
                                " Number of rounds: ${_rounds}; Public: ${_isOpen}; Team size: ${_teamSize}.")
                    }
                }
            } else {
                result = generateResponseMessage(true, "Can only modify pending games!")
                if (logger.isDebugEnabled()) {
                    logger.debug("Can only modify pending games!")
                }
            }
        } else {
            result = generateResponseMessage(true, "Game: ${game['gameName']} cannot be modified; player: ${playerName} is not the creator.")
            if (logger.isDebugEnabled()) {
                logger.debug("Game: ${game['gameName']} cannot be modified; player: ${playerName} is not the creator.")
            }
        }
        result
    }

    private findGameById(_gameId) {
        def game = gameDao.findGameById(_gameId)
        game
    }

    def quitGame(_gameId, playerName) {
        def result
        _gameId = new ObjectId(_gameId)
        def game = findGameById(_gameId)

        if (game != null) {
            if (game["playing"].contains(playerName)) {
                if (game["status"] == "PENDING") {
                    if (game["isOpen"] == false) {
                        //owner quitting a private, pending game ends the game altogether
                        if (game["createdBy"] == playerName) {
                            gameDao.deleteGame(_gameId)
                            result = generateResponseMessage(false, "Player: ${playerName} quit game: ${game['gameName']}; game successfully ended!")
                            if (logger.isDebugEnabled()) {
                                logger.debug("Player: ${playerName} quit game: ${game['gameName']}; game successfully ended!")
                            }
                        }
                        //non-owner quitting a private, pending game
                        else {
                            gameDao.quitGame(_gameId, playerName)
                            result = generateResponseMessage(false, "Player: ${playerName} successfully quit game: ${game['gameName']}!")
                            if (logger.isDebugEnabled()) {
                                logger.debug("Player: ${playerName} successfully quit game: ${game['gameName']}!")
                            }
                        }
                    }
                    //quitting a public, pending game; even if the owner quits, the game may continue.
                    else {
                        gameDao.quitGame(_gameId, playerName)
                        result = generateResponseMessage(false, "Player: ${playerName} successfully quit game: ${game['gameName']}!")
                        if (logger.isDebugEnabled()) {
                            logger.debug("Player: ${playerName} successfully quit game: ${game['gameName']}!")
                        }
                    }
                }
                //quitting an active game
                else if (game["status"] == "ACTIVE") {
                    //if there are 2 players and 1 is quitting, the remaining player is the default winner
                    if (game["playing"].size() == 2) {
                        game["playing"].remove(playerName)
                        def winner = game["playing"].get(0)
                        gameDao.winGame(_gameId, winner)
                        result = generateResponseMessage(false, "Player: ${playerName} quit; Game: ${game['gameName']} won by ${winner}!")
                        if (logger.isDebugEnabled()) {
                            logger.debug("Player: ${playerName} quit; Game: ${game['gameName']} won by ${winner}!")
                        }
                    } else {
                        gameDao.quitGame(_gameId, playerName)
                        answerDao.quitGame(_gameId, playerName)
                        voteDao.quitGame(_gameId, playerName)
                        result = generateResponseMessage(false, "Player: ${playerName} successfully quit game: ${game['gameName']}.")
                        if (logger.isDebugEnabled()) {
                            logger.debug("Player: ${playerName} successfully quit game: ${game['gameName']}.")
                        }
                    }
                } else {
                    result = generateResponseMessage(true, "Game: ${game['gameName']} is already finished!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Game: ${game['gameName']} is already finished!")
                    }
                }
            } else {
                result = generateResponseMessage(true, "Player: ${playerName} not found in game: ${game['gameName']}!")
                if (logger.isDebugEnabled()) {
                    logger.debug("Player: ${playerName} not found in game: ${game['gameName']}!")
                }
            }
        } else {
            result = generateResponseMessage(true, "Game not found!")
            if (logger.isDebugEnabled()) {
                logger.debug("Game not found!")
            }
        }
        result
    }

    def declineInvite(_gameId, playerName) {
        def result
        _gameId = new ObjectId(_gameId)
        def game = findGameById(_gameId)

        if (game != null) {
            if (game["pending"].contains(playerName)) {
                gameDao.declineInvite(_gameId, playerName)
                result = generateResponseMessage(false, "Player: ${playerName} declined game: ${game['gameName']}.")
                if (logger.isDebugEnabled()) {
                    logger.debug("Player: ${playerName} declined game: ${game['gameName']}.")
                }
            } else {
                result = generateResponseMessage(true, "Player: ${playerName} not invited to game: ${game['gameName']}!")
                if (logger.isDebugEnabled()) {
                    logger.debug("Player: ${playerName} not invited to game: ${game['gameName']}!")
                }
            }
        } else {
            result = generateResponseMessage(true, "Game not found!")
            if (logger.isDebugEnabled()) {
                logger.debug("Game not found!")
            }
        }
        result
    }

    def joinGame(_gameId, playerName) {
        def result
        _gameId = new ObjectId(_gameId)
        def game = findGameById(_gameId)

        if (game != null) {
            if (game["status"] == "PENDING") {
                //too many players now
                if (game["playing"].size() > game["numPlayers"]) {
                    gameDao.removePlayer(_gameId, playerName)
                    result = generateResponseMessage(true, "No space for player: ${playerName} in game: ${game['gameName']}!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("No space for player: ${playerName} in game: ${game['gameName']}!")
                    }
                }
                //uninvited to private game
                else if (game["isOpen"] == false && !game["pending"].contains(playerName)) {
                    gameDao.removePlayer(_gameId, playerName)
                    result = generateResponseMessage(true, "Player: ${playerName} was not invited to private game: ${game['gameName']}!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Player: ${playerName} was not invited to private game: ${game['gameName']}!")
                    }
                }
                //game is now full
                else if (game["playing"].size() == game["numPlayers"]) {
                    gameDao.startGame(_gameId)
                    result = generateResponseMessage(false, "Game: ${game['gameName']} has started now that player: ${playerName} has joined!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Game: ${game['gameName']} has started now that player: ${playerName} has joined!")
                    }
                } else {
                    result = generateResponseMessage(false, "Player: ${playerName} added to game: ${game['gameName']}.")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Player: ${playerName} added to game: ${game['gameName']}.")
                    }
                }
            } else {
                gameDao.removePlayer(_gameId, playerName)
                result = generateResponseMessage(true, "Game: ${game['gameName']} has status: ${game['status']}; player: ${playerName} may not join now!")
                if (logger.isDebugEnabled()) {
                    logger.debug("Game: ${game['gameName']} has status: ${game['status']}; player: ${playerName} may not join now!")
                }
            }
        } else {
            result = generateResponseMessage(true, "Game not found!")
            if (logger.isDebugEnabled()) {
                logger.debug("Game not found!")
            }
        }

        result
    }

    def getUserGamesByStatus(playerName, _status) {
        def games = gameDao.findUserGamesByStatus(playerName, _status)
        //if(logger.isDebugEnabled()) { logger.debug("fetched")}
        generateJSONRepresentation(games)
    }

    def findInvitedGames(playerName) {
        def games = gameDao.findInvitedGames(playerName)
        generateJSONRepresentation(games)
    }

    def deleteGame(_gameId, playerName) {
        def result
        _gameId = new ObjectId(_gameId)
        def game = findGameById(_gameId)

        if (game != null) {
            if (game["createdBy"] == playerName) {
                if (game["status"] == "PENDING") {
                    gameDao.deleteGame(_gameId)
                    result = generateResponseMessage(false, "Game: ${game['gameName']} has been removed by player: ${playerName}!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Game: ${game['gameName']} has been removed by player: ${playerName}!")
                    }
                } else {
                    result = generateResponseMessage(true, "Only unstarted games can be removed!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Only unstarted games can be removed!")
                    }
                }
            } else {
                result = generateResponseMessage(true, "Games can only be removed by its creator!")
                if (logger.isDebugEnabled()) {
                    logger.debug("Games can only be removed by its creator!")
                }
            }
        } else {
            result = generateResponseMessage(true, "Game does not exist!")
            if (logger.isDebugEnabled()) {
                logger.debug("Game does not exist!")
            }
        }

        result
    }

    def startNextRound(_gameId) {
        _gameId = new ObjectId(_gameId)
        def game = findGameById(_gameId)
        def result

        if (game != null) {
            if (game["status"] == "ACTIVE") {
                def rounds = game["rounds"]
                def _currentRound = game["currentRound"]

                //that was the final round
                if (_currentRound == rounds) {
                    determineWinner(game)
                    result = generateResponseMessage(false, "Game: ${game['gameName']} just finished!")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Game: ${game['gameName']} just finished!")
                    }
                } else {
                    def _letter = getRandomLetter()
                    def _categories = getRandomCategories()
                    _currentRound++
                    gameDao.startNextRound(_gameId, _letter, _categories)
                    result = generateResponseMessage(false, "Game: ${game['gameName']} advanced to round ${_currentRound}.")
                    if (logger.isDebugEnabled()) {
                        logger.debug("Game: ${game['gameName']} advanced to round ${_currentRound}.")
                    }
                }
            } else {
                result = generateResponseMessage(true, "Game: ${game['gameName']} is not active!")
                if (logger.isDebugEnabled()) {
                    logger.debug("Game: ${game['gameName']} is not active!")
                }
            }
        } else {
            result = generateResponseMessage(true, "Game does not exist!")
            if (logger.isDebugEnabled()) {
                logger.debug("Game does not exist!")
            }
        }

        result
    }

    private Map<Boolean, String> isValidInvite(_gameId, invitedName, inviterName) {
        def result = new HashMap<Boolean, JsonRepresentation>()
        def game = findGameById(_gameId)

        if (game != null) {
            if (game["status"] == "PENDING") {
                if (game["isOpen"] == true || game["createdBy"] == inviterName) {
                    if (inviterName != invitedName) {
                        def invitee = userDao.findUser(invitedName)
                        if (invitee != null) {
                            result.put(true, generateResponseMessage(false, "invite valid"))
                            if (logger.isDebugEnabled()) {
                                logger.debug("invite valid")
                            }
                        } else {
                            result.put(false, generateResponseMessage(true, "Player: ${invitedName} does not exist!"))
                            if (logger.isDebugEnabled()) {
                                logger.debug("Player: ${invitedName} does not exist!")
                            }
                        }
                    } else {
                        result.put(false, generateResponseMessage(true, "The creator is invited by default!"))
                        if (logger.isDebugEnabled()) {
                            logger.debug("The creator is invited by default!")
                        }
                    }
                } else {
                    result.put(false, generateResponseMessage(true, "Only the creator can modify invites in a private game!"))
                    if (logger.isDebugEnabled()) {
                        logger.debug("Only the creator can modify invites in a private game!")
                    }
                }
            } else {
                result.put(false, generateResponseMessage(true, "You cannot modify invites to a game that has already started!"))
                if (logger.isDebugEnabled()) {
                    logger.debug("You cannot modify invites to a game that has already started!")
                }
            }
        } else {
            result.put(false, generateResponseMessage(true, "Game does not exist!"))
            if (logger.isDebugEnabled()) {
                logger.debug("Game does not exist!")
            }
        }

        result
    }

    def inviteByUserName(_gameId, invitedName, inviterName) {
        def valid = isValidInvite(_gameId, invitedName, inviterName)
        def result

        if (valid.containsKey(true)) {
            gameDao.invite(_gameId, invitedName)
            gmail.inviteToPlay(_gameId, inviterName, invitedName)
            result = generateResponseMessage(false, "Player: ${invitedName} invited to play by: ${inviterName}.")
            if (logger.isDebugEnabled()) {
                logger.debug("Player: ${invitedName} invited to play by: ${inviterName}.")
            }
        } else
            result = valid.get(false)

        result
    }

    def inviteUserByEmail(_gameId, invitedEmail, inviterName) {
        def invitedName = userDao.findUserByEmail(invitedEmail)
        def valid = isValidInvite(_gameId, invitedName, inviterName)
        def result

        if (valid.containsKey(true)) {
            gameDao.invite(_gameId, invitedName)
            result = generateResponseMessage(false, "Player: ${invitedName} invited to play by: ${inviterName}.")
            if (logger.isDebugEnabled()) {
                logger.debug("Player: ${invitedName} invited to play by: ${inviterName}.")
            }
        } else
            result = valid.get(false)

        result
    }

    def unInviteUser(_gameId, uninvitedName, uninviterName) {
        def valid = isValidInvite(_gameId, uninvitedName, uninviterName)
        def result

        if (valid.containsKey(true)) {
            gameDao.uninvite(_gameId, uninvitedName)
            result = generateResponseMessage(false, "Player: ${uninvitedName} uninvited by: ${uninviterName}!")
            if (logger.isDebugEnabled()) {
                logger.debug("Player: ${uninvitedName} uninvited by: ${uninviterName}!")
            }
        } else
            result = valid.get(false)

        result
    }

    def getWinner(_gameId) {
        _gameId = new ObjectId(_gameId)
        def winners = gameDao.getWinners(_gameId)
        if (winners == null) winners = []
        generateJSONRepresentation(winners)
    }

    private void determineWinner(game) {
        def rounds = game["rounds"]
        def playerScore = [:]
        def players = game["playing"]

        players.each { playerName ->
            rounds.times { rd ->
                def answers = answerDao.getAnswersForPlayerByRound(game["_id"], playerName, rd + 1)
                def votes = voteDao.getVotesOnPlayerByRound(game["_id"], playerName, rd + 1)
                def score = calcScore(answers, votes)
                def currentScore = playerScore.get(playerName)
                currentScore == null ? playerScore.put(playerName, score) :
                    playerScore.put(playerName, currentScore + score)
            }
        }
        def maxScore = playerScore.values().max()
        def winners = []
        playerScore.keySet().each { playerName ->
            if (playerScore.get(playerName) == maxScore) winners.add(playerName)
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Game: ${game['gameName']} winners: ${winners}")
        }
        gameDao.winGame(game["_id"], winners)
    }

    private Integer calcScore(answers, votes) {
        def sums = []
        def index = 0

        votes.each { vote ->
            def point = (vote == true) ? 1 : -1
            sums[index] = (sums[index] == null) ? point : sums[index] + point
            (++index == answers.size()) ? index = 0 : index
        }
        def score = 0
        sums.each { value ->
            score += (value > 0) ? 1 : 0
        }

        score
    }

    private String getRandomLetter() {
        def letters = "A".."Z"
        def letter = letters[random.nextInt(letters.size())]

        if (logger.isDebugEnabled()) {
            logger.debug("generated random letter: ${letter}")
        }

        letter
    }

    private Set<String> getRandomCategories() {
        def all = gameDao.getCategories()
        def rand = [] as Set<String>

        while (rand.size() != 10) {
            rand.add(all[random.nextInt(all.size())])
        }
        if (logger.isDebugEnabled()) {
            logger.debug("generated random categories: ${rand}")
        }

        rand
    }
}
