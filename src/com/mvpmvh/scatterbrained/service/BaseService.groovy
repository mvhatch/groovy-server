package com.mvpmvh.scatterbrained.service

import org.json.JSONArray
import org.json.JSONObject
import org.restlet.ext.json.JsonRepresentation

abstract class BaseService {

    protected JsonRepresentation generateResponseMessage(failure, msg) {
        def JSONObject obj = new JSONObject()
        failure == true ? obj.put("ERROR", msg) : obj.put("SUCCESS", msg)
        new JsonRepresentation(obj)
    }

    protected JsonRepresentation generateJSONRepresentation(data) {
        if(data == null)
            new JsonRepresentation(new JSONObject.Null())
        else if(data instanceof java.util.Collection)
            new JsonRepresentation(new JSONArray(data))
        else
            new JsonRepresentation(new JSONObject(data))
    }

}
