package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.AnswerDao
import com.mvpmvh.scatterbrained.dao.GameDao
import com.mvpmvh.scatterbrained.dao.VoteDao
import org.apache.log4j.Logger
import org.bson.types.ObjectId

class VoteService extends BaseService {
    def AnswerDao answerDao
    def GameDao gameDao
    def VoteDao voteDao

    private static final Logger logger = Logger.getLogger(VoteService.class)

    def submitVotes(gameID, rd, playerName, voter, votes, finished) {
        def result
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)

        if(game != null) {
            def playing = game["playing"]
            def hasPlayer = playing.contains(playerName)
            def hasVoter = playing.contains(voter)

            if(hasPlayer && hasVoter) {
                voteDao.voteOnPlayer(playerName, gameID, rd, votes, voter, finished)
                def unfinishedVoters = voteDao.getUnfinishedVotersForRound(gameID, rd)
                def unfinishedAnswers = answerDao.getAllUnfinishedAnswersForRound(gameID, rd)

                //every player has submitted their answers for the round AND has voted for the round
                if (unfinishedAnswers != null && unfinishedAnswers.size() == 0 &&
                        unfinishedVoters != null && unfinishedVoters.size() == 0) {
                    result = generateResponseMessage(false, "Round is completed")
                    if(logger.isDebugEnabled()) {
                        logger.debug("The round is completed for game with id ${gameID}")
                    }
                }
                else {
                    result = generateResponseMessage(false, "Votes submitted")
                    if(logger.isDebugEnabled()) {
                        logger.debug("Player ${playerName} submitted votes for game id ${gameID} in round ${rd}")
                    }
                }
            }
            else {
                result = generateResponseMessage(true, "Cannnot vote on game ${gameID}. Player ${playerName} exists in game: ${hasPlayer} and voter: ${voter} exists in game: ${hasVoter}")
                if(logger.isDebugEnabled()) {
                    logger.debug("Cannnot vote on game ${gameID}. Player ${playerName} exists in game: ${hasPlayer} and voter: ${voter} exists in game: ${hasVoter}")
                }
            }
        }
        else {
            if (logger.isDebugEnabled()) {
                logger.debug("Game with id ${gameID} cannot be found!")
            }
            result = generateResponseMessage(true, "Game with id ${gameID} cannot be found!")
        }
        result
    }

    def readyToVote(gameID, playerName, rd) {
        def result
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)

        if(game != null) {
            def playing = game["playing"]
            def hasPlayer = playing.contains(playerName)

            if(hasPlayer) {
                def voters = voteDao.getWhoCanVote(gameID, rd)
                voteDao.addNewVoter(gameID, rd, playerName)

                if (voters != null && voters.size() > 0) {
                    voters.each { otherPlayer ->
                        voteDao.prepareToVoteOn(otherPlayer, gameID, rd, playerName)
                        voteDao.prepareToVoteOn(playerName, gameID, rd, otherPlayer)
                    }
                    result = generateResponseMessage(false, "Player: ${playerName} is ready to vote on the other players in game ${gameID} for round ${rd}")
                }
                else {
                    result = generateResponseMessage(false, "Player: ${playerName} is first ready to vote on game ${gameID} for round ${rd}")
                }
            }
            else {
                result = generateResponseMessage(true, "Player ${playerName} not found in game ${gameID}")
                if(logger.isDebugEnabled()) {
                    logger.debug("Player ${playerName} not found in game ${gameID}")
                }
            }
        }
        else {
            if (logger.isDebugEnabled()) {
                logger.debug("Game with id ${gameID} cannot be found!")
            }
            result = generateResponseMessage(true, "Game with id ${gameID} cannot be found!")
        }

        result
    }

    def getVotesForPlayerByRound(gameID, playerName, rd) {
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)
        def result

        if(game != null) {
            def playing = game["playing"]
            def hasPlayer = playing.contains(playerName)

            if(hasPlayer) {
                def votes = voteDao.getVotesOnPlayerByRound(playerName, gameID, rd)
                if(logger.isDebugEnabled()) {
                    logger.debug("Player ${playerName} fetched votes ${votes} for game ${gameID} in round ${rd}")
                }
                result = generateJSONRepresentation(votes)
            }
            else {
                result = generateResponseMessage(true, "Player ${playerName} not found in game ${gameID}")
                if(logger.isDebugEnabled()) {
                    logger.debug("Cannot get votes for player ${playerName}. Player does not exist in game ${gameID}!")
                }
            }
        }
        else {
            result = generateResponseMessage(true, "Game with id ${gameID} does not exist!")
            if(logger.isDebugEnabled()) {
                logger.debug("Game ${gameID} does not exist!")
            }
        }

        result
    }

    def getVotesForPlayerForGame(gameID, playerName) {
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)
        def result

        if(game != null) {
            if(game["playing"].contains(playerName)) {
                def votes = voteDao.getAllVotesOnPlayerForGame(playerName, gameID)
                result = generateJSONRepresentation(votes)
            }
            else {
                if(logger.isDebugEnabled()) {
                    logger.debug("Player ${playerName} not found in game ${gameID}")
                }
                result = generateResponseMessage(true, "Player ${playerName} not found in game ${gameID}")
            }
        }
        else {
            result = generateResponseMessage(true, "Game with id ${gameID} not found!")
            if(logger.isDebugEnabled()) {
                logger.debug("Game ${gameID} not found")
            }
        }

        result
    }

    def getVotesByPlayerForGame(gameID, voterName) {
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)
        def result

        if(game != null) {
            if(game["playing"].contains(voterName)) {
                def votes = voteDao.getAllPlayerVotesForGame(voterName, gameID)
                result = generateJSONRepresentation(votes)
            }
            else {
                if(logger.isDebugEnabled()) {
                    logger.debug("Voter ${voterName} not found in game ${gameID}")
                }
                result = generateResponseMessage(true, "Voter ${voterName} not found in game ${gameID}!")
            }
        }
        else {
            result = generateResponseMessage(true, "Game ${gameID} not found")
            if(logger.isDebugEnabled()) {
                logger.debug("Game ${gameID} not found")
            }
        }

        result
    }

    def getVotesByPlayerForGameByRound(voterName, gameID, rd) {
        gameID = new ObjectId(gameID)
        def result
        def game = gameDao.findGameById(gameID)

        if(game != null) {
            if(game["playing"].contains(voterName)) {
                def votes = voteDao.getPlayerVotesForGameByRound(voterName, gameID, rd)
                result = generateJSONRepresentation(votes)
            }
            else {
                result = generateResponseMessage(true, "Voter ${voterName} not found in game ${gameID}")
                if(logger.isDebugEnabled()) {
                    logger.debug("Voter ${voterName} not found in game ${gameID}")
                }
            }
        }
        else {
            result = generateResponseMessage(true, "Game ${gameID} not found!")
            if(logger.isDebugEnabled()) {
                logger.debug("Game ${gameID} not found")
            }
        }

        result
    }
}
