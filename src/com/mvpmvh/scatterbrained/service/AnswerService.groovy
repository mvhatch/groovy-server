package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.AnswerDao
import com.mvpmvh.scatterbrained.dao.GameDao
import org.apache.log4j.Logger
import org.bson.types.ObjectId

class AnswerService extends BaseService {
    def AnswerDao answerDao
    def GameDao gameDao

    private static final Logger logger = Logger.getLogger(AnswerService.class)
    
    def submitAnswers(gameID, playerName, rd, answers, finished) {
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)
        def result
        
        if(game != null) {
            if(game["playing"].contains(playerName)) {
                answerDao.saveAnswersForPlayerForRound(gameID, playerName, rd, answers, finished)
                if (finished)
                    result = generateResponseMessage(false, "Answers submitted, ready to vote")
                else
                    result = generateResponseMessage(false, "Answers submitted")
            }
            else {
                result = generateResponseMessage(true, "Player ${playerName} cannot submit answers to game they are not in!")
                if(logger.isDebugEnabled()) {
                    logger.debug("Player ${playerName} not in game ${gameID}; cannot submit answers for round ${rd}")
                }
            }
        }
        else {
            result = generateResponseMessage(true, "Game ${gameID} not found")
            if(logger.isDebugEnabled()) {
                logger.debug("Game ${gameID} not found")
            }
        }
        
        result           
    }

    def getPlayerAnswersForRound(gameID, playerName, rd) {
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)
        def result

        if(game != null) {
            if(game["playing"].contains(playerName)) {
                result = generateJSONRepresentation(answerDao.getAnswersForPlayerByRound(gameID, playerName, rd))
            }
            else {
                result = generateResponseMessage(true, "Player ${playerName} cannot retrieve answers to game they are not in!")
                if(logger.isDebugEnabled()) {
                    logger.debug("Player ${playerName} not in game ${gameID}; cannot retrieve answers for round ${rd}")
                }
            }
        }
        else {
            result = generateResponseMessage(true, "Game ${gameID} not found")
            if(logger.isDebugEnabled()) {
                logger.debug("Game ${gameID} not found")
            }
        }

        result
    }

    def getUnfinishedAnswersForRound(gameID, rd) {
        gameID = new ObjectId(gameID)
        def game = gameDao.findGameById(gameID)
        def result

        if(game != null) {
            result = generateJSONRepresentation(answerDao.getAllUnfinishedAnswersForRound(gameID, rd))
        }
        else {
            result = generateResponseMessage(true, "Game ${gameID} not found")
            if(logger.isDebugEnabled()) {
                logger.debug("Game ${gameID} not found")
            }
        }

        result
    }
}
