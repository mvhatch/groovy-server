package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.UserDao
import com.mvpmvh.scatterbrained.domain.BCrypt
import com.mvpmvh.scatterbrained.domain.Gmail
import org.apache.log4j.Logger

class UserService extends BaseService {
    def UserDao userDao
    def Gmail gmail

    private static final Logger logger = Logger.getLogger(UserService.class)

    def createUser(userName, _email, _password) {
        def user = userDao.findUser(userName)
        def result

        if (user == null) {    //the username is available
            if (userName != null && _email != null && _password != null) {
                _password = BCrypt.hashpw(_password, BCrypt.gensalt())
                def newUser = [username: userName, password: _password, email: _email, buddies: []]
                if (logger.isDebugEnabled()) {
                    logger.debug("saving new user: ${newUser}")
                }
                //TODO: send an email to new users thanking them for joining/inviting friends
                userDao.addUser(newUser)
                result = generateResponseMessage(false, "New user created")
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("failed to create new account: {usernme: ${userName}, email: ${_email}, password: ${_password}")
                }
                result = generateResponseMessage(true, "All fields are required!")
            }
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("cannot create new account: username: ${userName} already exists!")
            }
            result = generateResponseMessage(true, "Sorry, someone already has that username!")
        }
        result
    }

    def login(userName, _password) {
        def result

        if (userName != null && _password != null) {
            def userPassword = userDao.findPassword(userName)

            if (userPassword == null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("user was not found!")
                }
                result = generateResponseMessage(true, "Invalid login credentials!")
            } else {
                if (BCrypt.checkpw(_password, userPassword)) {
                    //TODO: return auth cookie?
                    if (logger.isDebugEnabled()) {
                        logger.debug("user is logged in")
                    }
                    result = generateResponseMessage(false, "Logged in.")
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("user was found but provided invalid credentials!")
                    }
                    result = generateResponseMessage(true, "Invalid login credentials!")
                }
            }
        } else
            result = generateResponseMessage(true, "Missing login credentials!")

        result
    }

    def updateUser(userName, _email, _password) {
        def result

        if (userName != null) {
            if (_email != null)
                userDao.updateUserEmail(userName, _email)
            if (_password != null) {
                _password = BCrypt.hashpw(_password, BCrypt.gensalt())
                userDao.updatePassword(userName, _password)
            }
            result = generateResponseMessage(false, "Player ${userName} updated")
        } else
            result = generateResponseMessage(true, "Missing username")

        result
    }

    def addBuddy(userName, buddyName) {
        def result

        if (userName != null && buddyName != null) {
            def buddyUser = userDao.findUser(buddyName)

            if (buddyName != userName) {    //you cannot add yourself as a buddy
                if (buddyUser != null) {    //make sure the buddy is an existing user
                    userDao.addBuddy(userName, buddyName)
                    result = generateResponseMessage(false, "Player ${buddyName} successfully added to buddies")
                } else
                    result = generateResponseMessage(true, "Player ${buddyName} does not exist!")
            } else
                result = generateResponseMessage(true, "You're already your own buddy, silly!")
        } else
            result = generateResponseMessage(true, "Cannot add with ${userName} and ${buddyName} as values")

        result
    }

    def removeBuddy(userName, buddyName) {
        def result

        if (userName != null && buddyName != null) {
            userDao.removeBuddy(userName, buddyName)
            result = generateResponseMessage(false, "Player ${buddyName} removed from Player ${userName} buddy list")
        } else
            result = generateResponseMessage(true, "Invalid parameters username: ${userName} and buddyName: ${buddyName}")

        result
    }

    def reset(userName) {
        def result

        if (userName != null) {
            def email = userDao.findEmail(userName)

            if (email != null) {
                def chars = 'A'..'Z'
                def chars2 = 'a'..'z'
                def chars3 = 0..9
                def charsTotal = chars + chars2 + chars3

                def randomPassword = ""
                10.times {
                    randomPassword += charsTotal[(Integer) Math.random() * charsTotal.size()]
                }

                def subj = "Your ScatterBrained password has been reset!"
                def body = "Dear ${userName}, \n Your new password is: ${randomPassword}"
                gmail.simpleMail(email, subj, body)
                userDao.updatePassword(userName, randomPassword)
                result = generateResponseMessage(false, "Player ${userName} password was reset")
            } else
                result = generateResponseMessage(true, "Player ${userName} email not found!")
        } else
            result = generateResponseMessage(true, "Missing username")

        result
    }

    def getUser(userName) {
        def response
        if (userName == null) {    //no name was provided; return all users in DB
            def userList = userDao.findAllUsers()
            response = generateJSONRepresentation(userList)
        } else {
            def user = userDao.findUser(userName)
            if (user != null) response = generateJSONRepresentation(user)
            else response = generateJSONRepresentation([])
        }

        response
    }

    def userExists(userName) {
        def rep
        def user = userDao.findUser(userName)
        if (user != null) rep = generateResponseMessage(false, "Player: ${userName} does exist")
        else rep = generateResponseMessage(true, "Player: ${userName} does not exist")
        rep
    }

    def forgotUsername(email) {
        def result

        if (email != null) {
            def username = userDao.findUserByEmail(email)
            if (username != null) {
                def subj = "Your ScatterBrained username has been requested!"
                def body = "Dear ${username}, \n That is your username."
                gmail.simpleMail(email, subj, body)
                result = generateResponseMessage(false, "Email: ${email} recovered username: ${username}")
            } else
                result = generateResponseMessage(true, "No user found with email: ${email}")
        } else
            result = generateResponseMessage(true, "No email provided")

        result
    }
}
