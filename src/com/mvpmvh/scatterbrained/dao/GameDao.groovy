package com.mvpmvh.scatterbrained.dao

class GameDao extends BaseDao {

    def findGameById(_gameId) {
        getMongoDB().games.findOne([_id: _gameId], [_id: 0])
    }

    def createGame(game) {
        getMongoDB().games.save(game)
    }

    def updateGame(_gameId, _numPlayers, _rounds, _isOpen, _teamSize) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId],
                    [$set: [numPlayers: _numPlayers, rounds: _rounds, isOpen: _isOpen, teamSize: _teamSize]])
        }
    }

    def deleteGame(_gameId) {
        getMongoDB() inRequest {
            getMongoDB().games.remove([_id: _gameId])
        }
    }

    def startGame(_gameId) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId], [$set: [status: "ACTIVE"], $unset: [pending: ""]])
        }
    }

    def winGame(_gameId, playerNames) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId],
                    [$pushAll: [winners: playerNames], $set: [status: "FINISHED"]])
        }
    }

    def quitGame(_gameId, playerName) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId], [$pull: [playing: playerName]])
        }
    }

    def invite(_gameId, playerName) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId], [$addToSet: [pending: playerName]])
        }
    }

    def uninvite(_gameId, playerName) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId], [$pull: [pending: playerName]])
        }
    }

    def declineInvite(_gameId, playerName) {
        getMongoDB.inRequest {
            getMongoDB().games.update([_id: _gameId], [$pull: [pending: playerName]])
        }
    }

    def startNextRound(_gameId, _letter, _categories) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId],
                    [$set: [letter: _letter, categories: _categories], $inc: [currentRound: 1]])
        }
    }

    def joinGame(_gameId, playerName) {
        getMongoDB().inRequest {
            getMongoDB().games.findAndModify([_id: _gameId, status: "PENDING"], [:], [:], false,
                    [$addToSet: [playing: playerName], $pull: [pending: playerName]], true, false)
        }
    }

    def removePlayer(_gameId, playerName) {
        getMongoDB().inRequest {
            getMongoDB().games.update([_id: _gameId], [$pull: [playing: playerName, pending: playerName]])
        }
    }

    def findUserGamesByStatus(playerName, _status) {
        getMongoDB().games.find(playing: [$in: [playerName]], status: _status).toArray()
    }

    def findInvitedGames(playerName) {
        getMongoDB().games.find(pending: [$in: [playerName]]).toArray()
    }

    def getCategories() {
        getMongoDB().categories.find().toArray()
    }

    def getWinners(_gameId) {
        getMongoDB().games.findOne([_id: _gameId], [winners: 1])?.winners
    }

}
