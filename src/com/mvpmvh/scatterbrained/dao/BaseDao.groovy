package com.mvpmvh.scatterbrained.dao

import com.gmongo.GMongo
import com.mongodb.DBAddress
import com.mongodb.WriteConcern

abstract class BaseDao {
    final dbName = "ScatterBrained"
    def GMongo gmongo



    protected getMongoDB() {
        if (gmongo == null) {
            gmongo = new GMongo(new DBAddress('localhost', 27017, dbName))
            gmongo.setWriteConcern(WriteConcern.SAFE)
        }
        gmongo.getDB(dbName)
    }
}
