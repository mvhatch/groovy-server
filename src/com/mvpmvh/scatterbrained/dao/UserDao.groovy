package com.mvpmvh.scatterbrained.dao

class UserDao extends BaseDao {

    def findUser(userName) {
        getMongoDB().users.findOne([username: userName], [_id: 0])
    }

    def findUser(userName, _password) {
        getMongoDB().users.findOne([username: userName, password: _password], [_id: 0])
    }

    //TODO: create findPasswordTest
    def findPassword(userName) {
        getMongoDB().users.findOne([username: userName], [_id: 0, password: 1])?.password
    }

    def findAllUsers() {
        getMongoDB().users.find().toArray()
    }

    def addUser(user) {
        getMongoDB().inRequest {
            getMongoDB().users.save(user)
        }
    }

    def updateUserEmail(userName, _email) {
        getMongoDB().inRequest {
            getMongoDB().users.update([username: userName], [$set: [email: _email]], true)
        }
    }

    def updatePassword(userName, _password) {
        getMongoDB().inRequest {
            getMongoDB().users.update([username: userName], [$set: [password: _password]], true)
        }
    }

    def addBuddy(userName, buddyName) {
        getMongoDB().inRequest {
            getMongoDB().users.update([username: userName], [$addToSet: [buddies: buddyName]])
        }
    }

    def removeBuddy(userName, buddyName) {
        getMongoDB().inRequest {
            getMongoDB().users.update([username: userName], [$pull: [buddies: buddyName]])
        }
    }

    //email must be unique
    def findEmail(userName) {
        getMongoDB().users.findOne([username: userName], [email: 1, _id: 0])?.email
    }

    //email must be unique
    def findUserByEmail(_email) {
        getMongoDB().users.findOne([email: _email], [username: 1, _id: 0])?.username
    }
}

