package com.mvpmvh.scatterbrained.dao

class AnswerDao extends BaseDao {

    def getAnswersForPlayerByRound(_gameId, playerName, rd) {
        getMongoDB().answers.findOne([gameId: _gameId, username: playerName, _round: rd], [answers: 1])
    }

    def saveAnswersForPlayerForRound(_gameId, playerName, rd, _answers, _finished) {
        getMongoDB().inRequest {
            getMongoDB().answers.update([gameId: _gameId, username: playerName, _round: rd],
                    [$set: [answers: _answers, finished: _finished]], true)
        }
    }

    def quitGame(_gameId, playerName) {
        getMongoDB().inRequest {
            getMongoDB().answers.remove([gameId: _gameId, username: playerName])
        }
    }

    def getAllUnfinishedAnswersForRound(_gameId, rd) {
        getMongoDB().answers.find([gameId: _gameId, _round: rd, finished: false]).toArray()
    }
}
