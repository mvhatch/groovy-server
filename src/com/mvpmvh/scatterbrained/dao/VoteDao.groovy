package com.mvpmvh.scatterbrained.dao

class VoteDao extends BaseDao {

    def getPlayerVotesForGameByRound(voterName, _gameId, rd) {
        getMongoDB().votes.find([voter: voterName, gameId: _gameId, _round: rd], [_id: 0]).toArray()
    }

    def getAllPlayerVotesForGame(voterName, _gameId) {
        getMongoDB().votes.find([voter: voterName, gameId: _gameId], [_id: 0]).toArray()
    }

    def voteOnPlayer(playerName, _gameId, rd, _votes, _voter, _finished) {
        getMongoDB().inRequest {
            getMongoDB().votes.update([gameId: _gameId, _round: rd, voter: _voter, votedOn: playerName],
                    [$set: [votes: _votes, finished: _finished]], true)
        }
    }

    def quitGame(_gameId, playerName) {
        getMongoDB().inRequest {
            getMongoDB().votes.remove([gameId: _gameId, votedOn: playerName])
            getMongoDB().votes.remove([gameId: _gameId, voter: playerName])
        }
    }

    def getVotesOnPlayerByRound(playerName, _gameId, rd) {
        getMongoDB().votes.find([gameId: _gameId, votedOn: playerName, _round: rd], [votes: 1]).toArray()
    }

    def getAllVotesOnPlayerForGame(playerName, _gameId) {
        getMongoDB().votes.find([votedOn: playerName, gameId: _gameId], [_id: 0]).toArray()
    }

    def getPlayerUnfinishedVotingListForRound(_gameId, voterName, rd) {
        getMongoDB().votes.find([gameId: _gameId, voter: voterName, _round: rd, finished: false]).toArray()
    }

    def getPlayerFinishedVotingListForRound(_gameId, voterName, rd) {
        getMongoDB().votes.find([gameId: _gameId, voter: voterName, _round: rd, finished: true]).toArray()
    }

    def getPlayerTotalVotingListForRound(_gameId, voterName, rd) {
        getMongoDB().votes.find([gameId: _gameId, voter: voterName, _round: rd]).toArray()
    }

    def getWhoCanVote(_gameId, rd) {
        getMongoDB().votes.distinct("voter", [gameId: _gameId, _round: rd])
    }

    def addNewVoter(_gameId, rd, playerName) {
        getMongoDB().inRequest {
            getMongoDB().votes.insert([gameId: _gameId, _round: rd, voter: playerName])
        }
    }

    def prepareToVoteOn(otherPlayer, _gameId, rd, playerName) {
        getMongoDB().votes.insert([gameId: _gameId, _round: rd, voter: playerName, votedOn: otherPlayer,
                votes: [], finished: false])
    }

    def getUnfinishedVotersForRound(_gameId, rd) {
        getMongoDB().votes.distinct("voter", [gameId: _gameId, _round: rd, finished: false]).toArray()
    }
}
