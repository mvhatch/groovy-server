package com.mvpmvh.scatterbrained.client

import com.gmongo.GMongo
import com.mongodb.DBAddress
import com.mongodb.WriteConcern

class DBClient {
    static GMongo gmongo = new GMongo(new DBAddress('localhost', 27017, "ScatterBrained"))

    static void main(args) {
        gmongo.setWriteConcern(WriteConcern.SAFE)
        def db = gmongo.getDB("ScatterBrained")

        new File("categories.txt").eachLine { category ->
            db.categories.insert(category: "${category}")
        }
    }
}
