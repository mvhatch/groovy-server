package com.mvpmvh.scatterbrained.client

import org.apache.http.HttpResponse
import org.apache.http.NameValuePair
import org.apache.http.client.HttpClient
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicNameValuePair

//import org.restlet.engine.adapter.HttpResponse

class LocalClient {
    static final BASE_URL = "http://localhost:80/ScatterBrainedServer"
    static player

    private static boolean tryLogin(username, pass) {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        nameValuePairs.add(new BasicNameValuePair("username", username))
        nameValuePairs.add(new BasicNameValuePair("password", pass))
        def rep = executeRequest("post", "/user/login", nameValuePairs)

        if (rep.getStatusLine().getStatusCode() != 200) {
            //throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode())
            return false
        }
        true
    }

    private static void printResponse(entity) {
        BufferedReader br = new BufferedReader(new InputStreamReader((entity.getContent())))
        def output

        println("Output from Server .... \n");
        while ((output = br.readLine()) != null) {
            println(output);
        }
    }

    static void newUserScreen() {
        println "new username?"
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        def username = br.readLine()
        println "new password?"
        def pass = br.readLine()
        println "new email?"
        def email = br.readLine()

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        nameValuePairs.add(new BasicNameValuePair("username", username))
        nameValuePairs.add(new BasicNameValuePair("password", pass))
        nameValuePairs.add(new BasicNameValuePair("email", email))
        def rep = executeRequest("post", "/user/create", nameValuePairs)

        if (rep.getStatusLine().getStatusCode() == 200) {
            player = username
            gameScreen()
        } else
            homeScreen()
    }

    static HttpResponse executeRequest(type, restOfUrl, nvp) {
        HttpClient client = new DefaultHttpClient()
        HttpResponse response
        try {
            if (type == "post") {
                HttpPost httpPost = new HttpPost("${BASE_URL}" + restOfUrl)
                httpPost.setEntity(new UrlEncodedFormEntity(nvp))
                response = client.execute(httpPost)
                printResponse(response.getEntity())
            } else if (type == "get") {
                HttpGet httpGet = new HttpGet("${BASE_URL}" + restOfUrl)
                response = client.execute(httpGet)
                printResponse(response.getEntity())
            }
        }
        finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            client.getConnectionManager().shutdown()
        }
        response
    }

    static void loginScreen() {
        println "Login username?"
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        def username = br.readLine()
        println "Login password?"
        def pass = br.readLine()

        if (tryLogin(username, pass) == false) homeScreen()
        else {
            player = username
            gameScreen()
        }
    }

    static void gameScreen() {
        def input = ""
        def l = [* 1..12]

        while (l.contains(input) == false) {
            println "1) Create new game"
            println "2) View list of current games"
            println "3) View list of pending games"
            println "4) View list of game invites"
            println "5) Invite others to a pending game"
            println "6) Join a game you're invited to"
            println "7 Quit a game"
            println "8) Decline a game invite"
            println "9) Delete a game"
            println "10) Invite by email to a game"
            println "11) Uninvite to game"
            println "12) Quit"
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
            input = Integer.parseInt(br.readLine())
        }

        if (input == "1") newGameScreen()
        else if (input == "2") currentGamesScreen()
        else if (input == "3") pendingGamesScreen()
        else if (input == "4") invitedGamesScreen()
        else if (input == "5") inviteGameScreen()
        else if (input == "6") joinGameScreen()
        else if (input == "7") quitGameScreen()
        else if (input == "8") declineGameScreen()
        else if (input == "9") deleteGameScreen()
        else if (input == "10") inviteEmailGameScreen()
        else if (input == "11") uninviteGameScreen()
        else System.exit(0)
    }

    static void newGameScreen() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(6)
        println "Game name?"
        def gameName = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("gameName", gameName))
        println "Number of players?"
        def numPlayers = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("numPlayers", numPlayers))
        println "Number of rounds?"
        def numRounds = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("rounds", numRounds))
        println "Game is public? (T,F)"
        def isOpen = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("isOpen", isOpen))
        println "Team size?"
        def teamSize = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("teamSize", teamSize))
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        def rep = executeRequest("post", "/game/create", nameValuePairs)

        gameScreen()
    }

    static void inviteGameScreen() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        println "Game name?"
        def gameName = br.readLine()
        println "Player to invite?"
        def playerName = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("invited", playerName))
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        executeRequest("post", "/game/${gameName}/inviteUser", nameValuePairs)
        gameScreen()
    }

    static void inviteEmailGameScreen() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        println "Game name?"
        def gameName = br.readLine()
        println "Player email to invite?"
        def playerEmail = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("email", playerEmail))
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        executeRequest("post", "/game/${gameName}/inviteEmail", nameValuePairs)
        gameScreen()
    }

    static void uninviteGameScreen() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        println "Game name?"
        def gameName = br.readLine()
        println "Player email to invite?"
        def playerName = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("uninvited", playerName))
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        executeRequest("post", "/game/${gameName}/uninvite", nameValuePairs)
        gameScreen()
    }

    static void joinGameScreen() {
        println "Games you are invited to are..."
        executeRequest("get", "/games/${player}/INVITED", null)
        println "Name of game to join?"
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        def gameName = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        executeRequest("post", "/game/${gameName}/join", nameValuePairs)
        gameScreen()
    }

    static void quitGameScreen() {
        println "Name of game to quit?"
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        def gameName = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        executeRequest("post", "/game/${gameName}/quit", nameValuePairs)
        gameScreen()
    }

    static void declineGameScreen() {
        println "Games you are invited to are..."
        executeRequest("get", "/games/${player}/INVITED", null)
        println "Name of game to decline?"
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        def gameName = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        executeRequest("post", "/game/${gameName}/decline", nameValuePairs)
        gameScreen()
    }

    static void deleteGameScreen() {
        println "Pending games are..."
        executeRequest("get", "/games/${player}/PENDING", null)
        println "Name of game to delete?"
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2)
        def gameName = br.readLine()
        nameValuePairs.add(new BasicNameValuePair("playerName", player))
        executeRequest("post", "/game/${gameName}/delete", nameValuePairs)
        gameScreen()
    }

    static void currentGamesScreen() {
        println "Current games are..."
        executeRequest("get", "/games/${player}/CURRENT", null)
        gameScreen()
    }

    static void pendingGamesScreen() {
        println "Pending games are..."
        executeRequest("get", "/games/${player}/PENDING", null)
        gameScreen()
    }

    static void invitedGamesScreen() {
        println "Games you are invited to are..."
        executeRequest("get", "/games/${player}/INVITED", null)
        gameScreen()
    }

    static void homeScreen() {
        def input
        while (input != "1" && input != "2" && input != "3") {
            println "1) Create new user"
            println "2) Login"
            println "3) Quit"
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in))
            input = br.readLine()
        }

        if (input == "1") newUserScreen()
        else if (input == "2") loginScreen()
        else System.exit(0)
    }

    static void main(args) {
        homeScreen()
    }
}
