var username;

function showLogin() {
    $("#loginDiv").show();
}

function hideLogin() {
    $("#loginDiv").hide();
}

function showNewUser() {
    $("#signUpDiv").show();
}

function hideNewUser() {
    $("#signUpDiv").hide();
}

function tryLogin(_username, password) {
    var dataString = 'username='+ _username + '&password=' + password;
    $.ajax({
        type: "POST",
        url: "http://localhost/ScatterBrainedServer/user/login",
        dataType: 'json',
        data: dataString,
        async:false,
        success: function(data, textStatus, jqXHR) {
            console.log(jqXHR.responseText);
            $('#loginForm').hide();
            username = _username;
            window.location = "/game.html";
            //showGames(username, "CURRENT");
        },
        error: function(jqXHR, textStatus, errorThrow) {
            alert("There was an error: " + errorThrow + " " + textStatus + " " + jqXHR.responseText);
            var responseText = jQuery.parseJSON(jqXHR.responseText);
            console.log(responseText);
        }
    });
    return false;
}

function getLoggedUser() {
    return username;
}

function showGames(username, status) {
    $.ajax({
        type: "GET",
        url: 'http://localhost:80/ScatterBrainedServer/games/' + username + '/' + status,
        dataType: 'json',
        async:false,
        success: function(data, textStatus, jqXHR) {
            console.log(jqXHR.responseText);
        },
        error: function(jqXHR, textStatus, errorThrow) {
            alert("There was an error: " + errorThrow + " " + textStatus + " " + jqXHR.responseText);
            var responseText = jQuery.parseJSON(jqXHR.responseText);
            console.log(responseText);
        }
    });
    return false;
}

function showNewGameForm() {
    $("#new_game_form").show();
}

function hideNewGameForm() {
    $("#new_game_form").hide();
}


$(function  () {
    var shoesData = [{name:"Nike", price:199.00 }, {name:"Loafers", price:59.00 }, {name:"Wing Tip", price:259.00 }];
    //Get the HTML from the template   in the script tag
    var theTemplateScript = $("#shoe-template").html();

    //Compile the template
    var theTemplate = Handlebars.compile(theTemplateScript);
    $(".shoesNav").append (theTemplate(shoesData));

    //We pass the shoesData object to the compiled handleBars function
    // The function will insert all the values from the objects in their respective places in the HTML and returned HTML as a string. Then we use jQuery to append the resulting HTML string into the page
});
