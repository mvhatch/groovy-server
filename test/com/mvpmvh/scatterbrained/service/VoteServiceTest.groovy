package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.AnswerDao
import com.mvpmvh.scatterbrained.dao.GameDao
import com.mvpmvh.scatterbrained.dao.VoteDao
import org.bson.types.ObjectId
import org.json.JSONArray
import org.junit.Test

class VoteServiceTest {
    def VoteService voteService = new VoteService()
    def AnswerDao mockAnswerDao
    def GameDao mockGameDao
    def VoteDao mockVoteDao

    @Test
    void testSubmitVotesIncrementRd() {
        mockVoteDao = [voteOnPlayer: { p1, p2, p3, p4, p5, p6 -> }, getUnfinishedVotersForRound: { p1, p2 -> [] }] as VoteDao
        //mockAnswerController = [getUnfinishedAnswersForRound: { p1, p2 -> [] }] as AnswerService
        //mockGameController = [startNextRound: { p1 -> "startNextRound was called" }] as GameService
        mockAnswerDao = [getUnfinishedVotersForRound: {param1, param2 -> []}] as AnswerDao
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer", "fooVoter"]]}] as GameDao
        voteService.setVoteDao(mockVoteDao)
        voteService.setAnswerDao(mockAnswerDao)
        voteService.setGameDao(mockGameDao)

        def fooId = new ObjectId().toString()
        def result = voteService.submitVotes(fooId, 4, "fooPlayer", "fooVoter", [], true)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Round is completed"
    }

    @Test
    void testSubmitVotesNoIcrementRd() {
        mockVoteDao = [voteOnPlayer: { p1, p2, p3, p4, p5, p6 -> }, getUnfinishedVotersForRound: { p1, p2 -> null }] as VoteDao
        mockAnswerDao = [getUnfinishedVotersForRound: {param1, param2 -> [1]}] as AnswerDao
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer", "fooVoter"]]}] as GameDao
        voteService.setVoteDao(mockVoteDao)
        voteService.setAnswerDao(mockAnswerDao)
        voteService.setGameDao(mockGameDao)

        def fooId = new ObjectId().toString()
        def result = voteService.submitVotes(fooId, 4, "fooPlayer", "fooVoter", [], false)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Votes submitted"
    }

    @Test
    void testFirstPersonReadyToVote() {
        mockVoteDao = [getWhoCanVote: { param1, param2 -> []}, addNewVoter: { p1, p2, p3 -> null }, prepareToVoteOn: { p1, p2, p3, p4 -> }] as VoteDao
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer"]]}] as GameDao
        voteService.setVoteDao(mockVoteDao)
        voteService.setGameDao(mockGameDao)

        def fooId = new ObjectId().toString()
        def result = voteService.readyToVote(fooId, "fooPlayer", 1)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer is first ready to vote on game ${fooId} for round 1"
    }

    @Test
    void testMultiplePersonsReadyToVote() {
        mockVoteDao = [getWhoCanVote: { param1, param2 -> ["voter1","voter2"]}, addNewVoter: { p1, p2, p3 -> null }, prepareToVoteOn: { p1, p2, p3, p4 -> }] as VoteDao
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer"]]}] as GameDao
        voteService.setVoteDao(mockVoteDao)
        voteService.setGameDao(mockGameDao)

        def fooId = new ObjectId().toString()
        def result = voteService.readyToVote(fooId, "fooPlayer", 1)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer is ready to vote on the other players in game ${fooId} for round 1"
    }

    @Test
    void testGetVotesForPlayerByRound() {
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer"]]}] as GameDao
        mockVoteDao = [getVotesOnPlayerByRound: {playerName, gameID, rd-> [true,true,false,true]}] as VoteDao
        voteService.setGameDao(mockGameDao)
        voteService.setVoteDao(mockVoteDao)

        def fooId = new ObjectId().toString()
        def result = voteService.getVotesForPlayerByRound(fooId, "fooPlayer", 3)
        def json = result.getJsonArray()
        def expected = new JSONArray([true,true,false,true])
        def index = 0
        json.each{
            assert json.get(index) == expected.get(index)
            index++
        }
    }

    @Test
    void testGetVotesForPlayerForGame() {
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer"]]}] as GameDao
        def fooId = new ObjectId().toString()
        def voteData = [[voter:"someVoter", votedOn: "fooPlayer", gameId: fooId, _round:1, votes:[true,false,true]],
                        [voter:"someVoter", votedOn: "fooPlayer", gameId: fooId, _round:2, votes:[true,true,true]]]
        mockVoteDao = [getAllVotesOnPlayerForGame: {playerName, gameID -> voteData}] as VoteDao
        voteService.setGameDao(mockGameDao)
        voteService.setVoteDao(mockVoteDao)

        def result = voteService.getVotesForPlayerForGame(fooId,"fooPlayer")
        def json = result.getJsonArray()
        def expected = new JSONArray(voteData)
        def index = 0
        json.each{
            def innerList = json.get(index)
            assert innerList == expected.get(index)
            index++
        }
    }

    @Test
    void testGetVotesByPlayerForGame() {
        mockGameDao = [findGameById: {param1 -> [playing:["votingPlayer"]]}] as GameDao
        def fooId = new ObjectId().toString()
        def voteData = [[voter:"votingPlayer", votedOn: "someOtherPlayer", gameId: fooId, _round:1, votes:[true,false,true]],
                        [voter:"votingPlayer", votedOn: "someOtherPlayer", gameId: fooId, _round:2, votes:[true,true,true]]]
        mockVoteDao = [getAllPlayerVotesForGame: {playerName, gameID -> voteData}] as VoteDao
        voteService.setGameDao(mockGameDao)
        voteService.setVoteDao(mockVoteDao)

        def result = voteService.getVotesByPlayerForGame(fooId,"votingPlayer")
        def json = result.getJsonArray()
        def expected = new JSONArray(voteData)
        def index = 0
        json.each{
            def innerList = json.get(index)
            assert innerList == expected.get(index)
            index++
        }
    }

    @Test
    void testGetVotesByPlayerForGameByRound() {
        mockGameDao = [findGameById: {param1 -> [playing:["votingPlayer"]]}] as GameDao
        def fooId = new ObjectId().toString()
        def voteData = [[voter:"votingPlayer", votedOn: "someOtherPlayer", gameId: fooId, _round:2, votes:[true,true,true]],
                        [voter:"votingPlayer", votedOn: "someOtherPlayer2", gameId: fooId, _round:2, votes:[true,true,true]]]
        mockVoteDao = [getPlayerVotesForGameByRound: {votingName, gameID, rd -> voteData}] as VoteDao
        voteService.setGameDao(mockGameDao)
        voteService.setVoteDao(mockVoteDao)

        def result = voteService.getVotesByPlayerForGameByRound("votingPlayer", fooId, 2)
        def json = result.getJsonArray()
        def expected = new JSONArray(voteData)
        def index = 0
        json.each{
            def innerList = json.get(index)
            assert innerList == expected.get(index)
            index++
        }
    }
}
