package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.AnswerDao
import com.mvpmvh.scatterbrained.dao.GameDao
import org.bson.types.ObjectId
import org.junit.Test

class AnswerServiceTest {
    def AnswerService answerService = new AnswerService()
    def AnswerDao mockAnswerDao
    def GameDao mockGameDao

    @Test
    void testSubmitAnswersFinished() {
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer"]]}] as GameDao
        mockAnswerDao = [saveAnswersForPlayerForRound: { p1, p2, p3, p4, p5 -> }] as AnswerDao
        answerService.setAnswerDao(mockAnswerDao)
        answerService.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = answerService.submitAnswers(fooId, "fooPlayer", 4, [], true)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Answers submitted, ready to vote"
    }

    @Test
    void testSubmitAnswersUnFinished() {
        mockGameDao = [findGameById: {param1 -> [playing:["fooPlayer"]]}] as GameDao
        mockAnswerDao = [saveAnswersForPlayerForRound: { p1, p2, p3, p4, p5 -> }] as AnswerDao
        answerService.setAnswerDao(mockAnswerDao)
        answerService.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = answerService.submitAnswers(fooId, "fooPlayer", 4, [], false)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Answers submitted"
    }

    @Test
    void testGetUnfinishedAnswersForRound() {
        mockGameDao = [findGameById: {param1 -> [playing:["votingPlayer"]]}] as GameDao
        mockAnswerDao = [getUnfinishedAnswers: { p1, p2 -> [] }] as AnswerDao
        answerService.setAnswerDao(mockAnswerDao)
        answerService.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = answerService.getUnfinishedAnswersForRound(fooId, 4)
        def json = result.getJsonArray()
        println json
    }
}
