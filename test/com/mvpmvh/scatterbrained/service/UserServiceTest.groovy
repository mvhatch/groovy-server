package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.UserDao
import com.mvpmvh.scatterbrained.domain.BCrypt
import com.mvpmvh.scatterbrained.domain.Gmail
import groovy.mock.interceptor.MockFor
import org.junit.BeforeClass
import org.junit.Test

class UserServiceTest {
    def userService = new UserService()
    def mockUserDAO

    @BeforeClass
    static void setUp() {
        //userService = new userService()
    }

    @Test
    void testCreateUserSuccess() {
        /*TODO: make this mock framework...work
         *  mockUserDAO.demand.with{
            findUser() {userName -> null }
            addUser() {user -> null }
        }

        mockUserDAO.use{
            def result = userService.createUser("fooUser", "fooEmail", "fooPassword")
            assert result.has("SUCCESS") == true
        }*/
        mockUserDAO = [findUser: { param -> null }, addUser: { param -> null }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.createUser("fooUser", "fooEmail", "fooPassword")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "New user created"
    }

    @Test
    void testCreateUserFailUserExists() {
        def foundUser = [] //fake returned user
        mockUserDAO = [findUser: { param -> foundUser }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.createUser("fooUser", "fooEmail", "fooPassword")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Sorry, someone already has that username!"
    }

    @Test
    void testCreateUserNullParams() {
        mockUserDAO = [findUser: { param -> null }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.createUser(null, "fooEmail", "fooPassword")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "All fields are required!"
    }

    @Test
    void testLogin() {
        def mockBCrypt = new MockFor(BCrypt.class)
        mockUserDAO = [findPassword: {username -> "fooHashedPassword"}] as UserDao
        userService.setUserDao(mockUserDAO)

        mockBCrypt.demand.checkpw{param1, param2-> true}
        mockBCrypt.use {
            def result = userService.login("fooUser", "fooPassword")
            def json = result.getJsonObject()
            assert json.has("SUCCESS") == true
            assert json.get("SUCCESS") == "Logged in."
        }
    }

    @Test
    void testLoginNullParams() {
        def result = userService.login(null, "fooPassword")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Missing login credentials!"
    }

    @Test
    void testLoginInvalidParams() {
        mockUserDAO = [findUser: { param1, param2 -> null }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.login("fooUser", "fooPassword")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Invalid login credentials!"
    }

    @Test
    void testUpdateUser() {
        mockUserDAO = [updateUserEmail: { param1, param2 -> }, updatePassword: { param1, param2 -> }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.updateUser("fooUser", "foo@foo", "fooPass")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player fooUser updated"
    }

    @Test
    void testUpdateUserMissingUsername() {
        def result = userService.updateUser(null, "foo@foo", "fooPass")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Missing username"
    }

    @Test
    void testAddBuddy() {
        def fakeUser = []
        mockUserDAO = [findUser: { param1 -> fakeUser }, addBuddy: { param1, param2 -> }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.addBuddy("fooUser", "bazUser")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player bazUser successfully added to buddies"
    }

    @Test
    void testAddYourselfAsBuddyFail() {
        def fakeUser = []
        mockUserDAO = [findUser: { param1 -> fakeUser }, addBuddy: { param1, param2 -> }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.addBuddy("fooUser", "fooUser")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "You're already your own buddy, silly!"
    }

    @Test
    void testNonexistentBuddyFail() {
        mockUserDAO = [findUser: { param1 -> null }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.addBuddy("fooUser", "bazUser")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Player bazUser does not exist!"
    }

    @Test
    void testAddNullBuddyFail() {
        def result = userService.addBuddy("fooUser", null)
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Cannot add with fooUser and null as values"
    }

    @Test
    void testRemoveBuddy() {
        mockUserDAO = [removeBuddy: { param1, param2 -> }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.removeBuddy("fooUser", "bazUser")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player bazUser removed from Player fooUser buddy list"
    }

    @Test
    void testRemoveBuddyInvalidParams() {
        def result = userService.removeBuddy("fooUser", null)
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Invalid parameters username: fooUser and buddyName: null"
    }

    @Test
    void testReset() {
        mockUserDAO = [findEmail: { param1 -> "" }, updatePassword: { param1, param2 -> }] as UserDao
        def mockGmail = [simpleMail: { param1, param2, param3 -> }] as Gmail
        userService.setUserDao(mockUserDAO)
        userService.setGmail(mockGmail)
        def result = userService.reset("fooUser")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player fooUser password was reset"
    }

    @Test
    void testResetFindEmailFail() {
        mockUserDAO = [findEmail: { param1 -> null }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.reset("fooUser")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Player fooUser email not found!"
    }

    @Test
    void testResetMissingUsernameFail() {
        def result = userService.reset(null)
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Missing username"
    }

    @Test
    void testGetSpecificUser() {
        def fakeUser = [username: "foo", email: "foo@foo"]
        mockUserDAO = [findUser: { param -> fakeUser }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.getUser("foo")
        def json = result.getJsonObject()
        assert json.get("username") == fakeUser["username"]
        assert json.get("email") == fakeUser["email"]
    }

    @Test
    void testGetAllUsers() {
        def fakeUser1 = [username: "foo", email: "foo@foo"]
        def fakeUser2 = [username: "baz", email: "baz@baz"]
        def fakeUserList = [fakeUser1, fakeUser2]
        mockUserDAO = [findAllUsers: { fakeUserList }] as UserDao
        userService.setUserDao(mockUserDAO)
        def result = userService.getUser(null)
        def json = result.getJsonArray()
        assert json.length() == 2
        json.length().times { assert json.get(it) == fakeUserList[it] }
    }

    @Test
    void testForgotUsername() {
        mockUserDAO = [findUserByEmail: { param -> "fooUser" }] as UserDao
        def mockGmail = [simpleMail: { param1, param2, param3 -> }] as Gmail
        userService.setGmail(mockGmail)
        userService.setUserDao(mockUserDAO)
        def result = userService.forgotUsername("foo@foo")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Email: foo@foo recovered username: fooUser"

        mockUserDAO = [findUserByEmail: { param -> null }] as UserDao
        userService.setUserDao(mockUserDAO)
        result = userService.forgotUsername("foo@foo")
        json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "No user found with email: foo@foo"

        result = userService.forgotUsername(null)
        json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "No email provided"
    }
}
