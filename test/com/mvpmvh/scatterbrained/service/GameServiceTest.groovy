package com.mvpmvh.scatterbrained.service

import com.mvpmvh.scatterbrained.dao.AnswerDao
import com.mvpmvh.scatterbrained.dao.GameDao
import com.mvpmvh.scatterbrained.dao.UserDao
import com.mvpmvh.scatterbrained.dao.VoteDao
import com.mvpmvh.scatterbrained.domain.Gmail
import org.bson.types.ObjectId
import org.json.JSONArray
import org.junit.BeforeClass
import org.junit.Test

class GameServiceTest {
    def GameService gc = new GameService()
    def GameDao mockGameDao

    @BeforeClass
    static void setUp() {

    }

    @Test
    void testCreateGame() {
        def categories = ["song", "food", "sport", "celebrity", "location", "color", "apparel", "god", "beverage", "hero"]
        mockGameDao = [getCategories: categories, createGame: { param -> }] as GameDao
        gc.setGameDao(mockGameDao)
        Random r = new Random()
        gc.setRandom(r)
        def result = gc.createGame("fooGame", "fooPlayer", 3, 3, false, 1)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Game: fooGame successfully created!"
    }

    @Test
    void testCreateGameDuplicateName() {
        def categories = ["song", "food", "sport", "celebrity", "location", "color", "apparel", "god", "beverage", "hero"]
        mockGameDao = [getCategories: categories, createGame: { param -> }] as GameDao
        gc.setGameDao(mockGameDao)
        Random r = new Random()
        gc.setRandom(r)
        def result = gc.createGame("fooGame", "fooPlayer", 3, 3, false, 1)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Game: fooGame successfully created!"

        result = gc.createGame("fooGame", "fooPlayer", 3, 3, false, 1)
        json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Game: fooGame successfully created!"
    }

    @Test
    void testCreateGameInvalidParams() {
        mockGameDao = [findGameById: { param -> null }] as GameDao
        gc.setGameDao(mockGameDao)
        def result = gc.createGame("fooGame", "fooPlayer", 3, 3, false, 0)
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Invalid game options!"
    }

    @Test
    void testCreatorQuitPendingPrivateGame() {
        def game = [playing: ["fooPlayer"], status: "PENDING", isOpen: false, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, quitGame: { param -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "fooPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer quit game: fooGame; game successfully ended!"
    }

    @Test
    void testCreatorQuitPendingPublicGame() {
        def game = [playing: ["fooPlayer"], status: "PENDING", isOpen: true, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, quitGame: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "fooPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer successfully quit game: fooGame!"
    }

    @Test
    void testNonCreatorQuitPendingPrivateGame() {
        def game = [playing: ["fooPlayer", "barPlayer"], status: "PENDING", isOpen: false, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, quitGame: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "barPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: barPlayer successfully quit game: fooGame!"
    }

    @Test
    void testNonCreatorQuitPendingPublicGame() {
        def game = [playing: ["fooPlayer", "barPlayer"], status: "PENDING", isOpen: true, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, quitGame: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "barPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: barPlayer successfully quit game: fooGame!"
    }

    //TODO: create tests for updating games

    @Test
    void testQuitActiveGame2Players() {
        def game = [playing: ["fooPlayer", "barPlayer"], status: "ACTIVE", isOpen: true, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, winGame: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "barPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: barPlayer quit; Game: fooGame won by fooPlayer!"
    }

    @Test
    void testQuitActiveGameManyPlayers() {
        def game = [playing: ["fooPlayer", "barPlayer", "bazPlayer"], status: "ACTIVE", isOpen: false, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, quitGame: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def mockAnswerDao = [quitGame: { param1, param2 -> }] as AnswerDao
        gc.setAnswerDao(mockAnswerDao)
        def mockVoteDao = [quitGame: { param1, param2 -> }] as VoteDao
        gc.setVoteDao(mockVoteDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "fooPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer successfully quit game: fooGame."
    }

    @Test
    void testQuitFinishedGame() {
        def game = [playing: ["fooPlayer", "barPlayer", "bazPlayer"], status: "FINISHED", isOpen: false, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, quitGame: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "fooPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game: fooGame is already finished!"
    }

    @Test
    void testQuitUnknownPlayer() {
        def game = [playing: ["fooPlayer", "barPlayer"], status: "ACTIVE", isOpen: false, createdBy: "fooPlayer", gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }, quitGame: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "bazPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Player: bazPlayer not found in game: fooGame!"
    }

    @Test
    void testQuitUnknownGame() {
        mockGameDao = [findGameById: { param -> null }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.quitGame(fooId, "bazPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game not found!"
    }

    @Test
    void testDeclineInvite() {
        def game = [pending: ["bazPlayer", "fooPlayer"], gameName: "fooGame", playing: []]
        mockGameDao = [findGameById: { param -> game }, declineInvite: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.declineInvite(fooId, "bazPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: bazPlayer declined game: fooGame."
    }

    @Test
    void testUninvitedDeclineInvite() {
        def game = [pending: ["bazPlayer", "fooPlayer"], gameName: "fooGame"]
        mockGameDao = [findGameById: { param -> game }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.declineInvite(fooId, "barPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Player: barPlayer not invited to game: fooGame!"
    }

    @Test
    void testDeclineInviteUnknownGame() {
        mockGameDao = [findGameById: { param -> null }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.declineInvite(fooId, "barPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game not found!"
    }

    @Test
    void testJoinGameTooManyPlayers() {
        def game = [playing: ["fooPlayer", "bazPlayer", "joinedPlayer"], numPlayers: 2, gameName: "fooGame"]
        def mockGameDao = [findGameById: { param1 -> game }, removePlayer: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.joinGame(fooId, "joinedPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game: fooGame has status: null; player: joinedPlayer may not join now!"
    }

    @Test
    void testJoinPublicGameBecomesFull() {
        def game = [playing: ["fooPlayer", "bazPlayer", "joinedPlayer"], numPlayers: 3, isOpen: true, gameName: "fooGame", status: "PENDING"]
        def mockGameDao = [startGame: { param1 -> }, findGameById: {param1 -> game}] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.joinGame(fooId, "joinedPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Game: fooGame has started now that player: joinedPlayer has joined!"
    }

    @Test
    void testJoinPublicGameNotFull() {
        def game = [playing: ["fooPlayer", "bazPlayer", "joinedPlayer"], numPlayers: 4, isOpen: true, gameName: "fooGame", status: "PENDING"]
        def mockGameDao = [findGameById: {param1 -> game}] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.joinGame(fooId, "joinedPlayer")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: joinedPlayer added to game: fooGame."
    }

    @Test
    void testJoinPrivateGameNotInvited() {
        def game = [playing: ["fooPlayer", "bazPlayer", "joinedPlayer"], numPlayers: 4, isOpen: false, pending: ["barPlayer", "bar2Player"], gameName: "fooGame", status: "PENDING"]
        def mockGameDao = [findGameById: { param1 -> game }, removePlayer: { param1, param2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.joinGame(fooId, "unInvitedPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Player: unInvitedPlayer was not invited to private game: fooGame!"
    }

    @Test
    void testJoinPrivateGameNotFull() {
        def game = [playing: ["fooPlayer", "bazPlayer"], numPlayers: 4, isOpen: false, pending: ["barPlayer", "bar2Player"], gameName: "fooGame", status: "PENDING"]
        def mockGameDao = [removePlayer: { param1, param2 -> }, findGameById: {param1 -> game}] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.joinGame(fooId, "bar2Player")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: bar2Player added to game: fooGame."
    }

    @Test
    void testJoinGameNotFound() {
        def mockGameDao = [findGameById: { param1 -> null }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.joinGame(fooId, "joinedPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game not found!"
    }
    /*
    @Test
    void testGetUserGamesByStatus() {
        def game = [playing: ["fooPlayer", "bazPlayer", "barPlayer"], numPlayers: 4, isOpen: false, pending: ["barPlayer", "bar2Player"], status: "PENDING"]
        def mockGameDao = [findUserGamesByStatus: { param, param2 -> game }] as GameDao
        gc.setGameDao(mockGameDao)
        def result = gc.getUserGamesByStatus("fooPlayer", "PENDING")
        def json = result.getJsonArray()
        println json
    }*/

    @Test
    void testDeletePendingGameByCreator() {
        def game = [createdBy: "fooCreator", status: "PENDING", gameName: "fooGame"]
        def mockGameDao = [findGameById: { param -> game }, deleteGame: { param -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.deleteGame(fooId, "fooCreator")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Game: fooGame has been removed by player: fooCreator!"
    }

    @Test
    void testDeletePendingGameByNonCreator() {
        def game = [createdBy: "fooCreator", status: "PENDING"]
        def mockGameDao = [findGameById: { param -> game }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.deleteGame(fooId, "fooPlayer")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Games can only be removed by its creator!"
    }

    @Test
    void testDeleteNonPendingGame() {
        def game = [createdBy: "fooCreator", status: "ACTIVE"]
        def mockGameDao = [findGameById: { param -> game }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.deleteGame(fooId, "fooCreator")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Only unstarted games can be removed!"
    }

    @Test
    void testStartNextRoundGameNotFinished() {
        def categories = ["song", "food", "sport", "celebrity", "location", "color", "apparel", "god", "beverage", "hero"]
        def game = [createdBy: "fooCreator", status: "ACTIVE", rounds: 2, currentRound: 1, gameName: "fooGame"]
        def mockGameDao = [findGameById: { param -> game }, startNextRound: { param1, param2, param3 -> }, getCategories: categories] as GameDao
        gc.setGameDao(mockGameDao)
        Random r = new Random()
        gc.setRandom(r)
        def fooId = new ObjectId().toString()
        def result = gc.startNextRound(fooId)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Game: fooGame advanced to round 2."
    }

    @Test
    void testStartNextRoundInactiveGame() {
        def game = [createdBy: "fooCreator", status: "PENDING", rounds: 2, gameName: "fooGame"]
        def mockGameDao = [findGameById: { param -> game }, startNextRound: { param1, param2, param3 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        Random r = new Random()
        gc.setRandom(r)
        def fooId = new ObjectId().toString()
        def result = gc.startNextRound(fooId)
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game: fooGame is not active!"
    }

    @Test
    void testStartNextRoundFinishGame() {
        def game = [createdBy: "fooCreator", status: "ACTIVE", rounds: 1, currentRound: 1, playing: ["fooCreator", "barPlayer"], gameName: "fooGame"]
        def fooAnswers = ["a", "b", "c", "d"]
        def barAnswers = ["e", "f", "g", "h"]
        def fooVotes = [true, false, false, true]
        def barVotes = [false, false, false, false]
        def mockGameDao = [findGameById: { param -> game }, winGame: { p1, p2 -> }] as GameDao
        gc.setGameDao(mockGameDao)
        def mockAnswerDao = [getAnswersForPlayerByRound: { param1, param2, param3 ->
            if (param2 == "fooCreator") {
                return fooAnswers
            } else return barAnswers
        }] as AnswerDao
        def mockVoteDao = [getVotesOnPlayerByRound: { param1, param2, param3 ->
            if (param2 == "fooCreator") {
                return barVotes
            } else return fooVotes
        }] as VoteDao
        gc.setAnswerDao(mockAnswerDao)
        gc.setVoteDao(mockVoteDao)
        def fooId = new ObjectId().toString()
        def result = gc.startNextRound(fooId)
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Game: fooGame just finished!"
    }

    @Test
    void testStartNextRoundUnknownGame() {
        def mockGameDao = [findGameById: { param -> null }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.startNextRound(fooId)
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game does not exist!"
    }

    @Test
    void testInviteToOpenGameByUserName() {
        def game = [status: "PENDING", isOpen: true]
        def mockUserDao = [findUser: { param -> [] }] as UserDao
        def mockGameDao = [findGameById: { param -> game }, invite: { p1, p2 -> }] as GameDao
        def mockGmail = [inviteToPlay: { p1, p2, p3 -> }] as Gmail
        gc.setUserDao(mockUserDao)
        gc.setGameDao(mockGameDao)
        gc.setGmail(mockGmail)
        def fooId = new ObjectId().toString()
        def result = gc.inviteByUserName(fooId, "fooPlayer", "fooCreator")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer invited to play by: fooCreator."
    }

    @Test
    void testCreatorInviteToPrivateGameByUserName() {
        def game = [status: "PENDING", isOpen: false, createdBy: "fooCreator"]
        def mockUserDao = [findUser: { param -> [] }] as UserDao
        def mockGameDao = [findGameById: { param -> game }, invite: { p1, p2 -> }] as GameDao
        def mockGmail = [inviteToPlay: { p1, p2, p3 -> }] as Gmail
        gc.setUserDao(mockUserDao)
        gc.setGameDao(mockGameDao)
        gc.setGmail(mockGmail)
        def fooId = new ObjectId().toString()
        def result = gc.inviteByUserName(fooId, "fooPlayer", "fooCreator")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer invited to play by: fooCreator."
    }

    @Test
    void testNonCreatorInviteToPrivateGameByUserName() {
        def game = [status: "PENDING", isOpen: false, createdBy: "fooUser"]
        def mockUserDao = [findUser: { param -> [] }] as UserDao
        def mockGameDao = [findGameById: { param -> game }] as GameDao
        gc.setUserDao(mockUserDao)
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.inviteByUserName(fooId, "fooPlayer", "fooCreator")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Only the creator can modify invites in a private game!"

    }

    @Test
    void testCreatorInviteToNonPendinGameByUserName() {
        def game = [status: "ACTIVE", isOpen: false, createdBy: "fooCreator"]
        def mockUserDao = [findUser: { param -> [] }] as UserDao
        def mockGameDao = [findGameById: { param -> game }] as GameDao
        gc.setUserDao(mockUserDao)
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.inviteByUserName(fooId, "fooPlayer", "fooCreator")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "You cannot modify invites to a game that has already started!"
    }

    @Test
    void testCreatorInviteUnknownUser() {
        def game = [status: "PENDING", isOpen: false, createdBy: "fooCreator"]
        def mockUserDao = [findUser: { param -> null }] as UserDao
        def mockGameDao = [findGameById: { param -> game }] as GameDao
        gc.setUserDao(mockUserDao)
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId()
        def result = gc.inviteByUserName(fooId, "fooPlayer", "fooCreator")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Player: fooPlayer does not exist!"
    }

    @Test
    void testCreatorInviteUnknownGame() {
        def mockGameDao = [findGameById: { param -> null }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.inviteByUserName(fooId, "fooPlayer", "fooCreator")
        def json = result.getJsonObject()
        assert json.has("ERROR") == true
        assert json.get("ERROR") == "Game does not exist!"
    }

    @Test
    void testUnInviteUser() {
        def game = [status: "PENDING", isOpen: false, createdBy: "fooCreator"]
        def mockUserDao = [findUser: { param -> [] }] as UserDao
        def mockGameDao = [findGameById: { param -> game }, uninvite: { p1, p2 -> }] as GameDao
        gc.setUserDao(mockUserDao)
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.unInviteUser(fooId, "fooPlayer", "fooCreator")
        def json = result.getJsonObject()
        assert json.has("SUCCESS") == true
        assert json.get("SUCCESS") == "Player: fooPlayer uninvited by: fooCreator!"
    }

    @Test
    void testGetWinner() {
        def mockGameDao = [getWinners: { param -> ["p1", "p2"] }] as GameDao
        gc.setGameDao(mockGameDao)
        def fooId = new ObjectId().toString()
        def result = gc.getWinner(fooId)
        def json = result.getJsonArray()
        def expected = new JSONArray(["p1","p2"])
        def index = 0
        json.each{
            assert json.get(index) == expected.get(index)
            index++
        }
    }
}
