package com.mvpmvh.scatterbrained.dao

import com.gmongo.GMongo
import com.mongodb.DB
import com.mongodb.DBAddress
import com.mongodb.WriteConcern
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.Test

class GameDaoTest {

    static MockGameDAO gameDao = new MockGameDAO()

    @Before
    void setUp() {
        //gameDAO = new MockGameDAO()
    }

    @AfterClass
    static void tearDownAfterClass() {
        gameDao.getMongoDB().dropDatabase()
    }

    @After
    void tearDown() {
        gameDao.getMongoDB().games.drop()
    }

    @Test
    void testFindGameById() {
        def game = [gameName: "fooGame"]
        gameDao.createGame(game)
        def gameId = game._id
        assert gameId != null
        def savedGame = gameDao.findGameById(gameId)
        assert savedGame["gameName"] == "fooGame"
    }

    @Test
    void testUpdateGame() {
        def game = [gameName: "fooGame1", numPlayers: 3, rounds: 1, isOpen: false, teamSize: 1]
        gameDao.createGame(game)
        def gameId = game._id
        assert gameDao.findGameById(gameId).rounds == 1
        assert gameDao.findGameById(gameId).numPlayers == 3
        assert gameDao.findGameById(gameId).isOpen == false

        gameDao.updateGame(gameId, 2, 4, true, 1)
        assert gameDao.findGameById(gameId).rounds == 4
        assert gameDao.findGameById(gameId).numPlayers == 2
        assert gameDao.findGameById(gameId).isOpen == true
        assert gameDao.findGameById(gameId).teamSize == 1
    }

    @Test
    void testDeleteGame() {
        def game = [gameName: "fooGame3"]
        gameDao.createGame(game)
        def gameId = game._id
        assert gameDao.findGameById(gameId) != null
        gameDao.deleteGame(gameId)
        assert gameDao.findGameById(gameId) == null
    }

    @Test
    void testStartGame() {
        def game = [gameName: "fooGame2", numPlayers: 3, rounds: 1, isOpen: false, teamSize: 1, status: "PENDING", pending: ["p1", "p2"]]
        gameDao.createGame(game)
        def gameId = game._id
        gameDao.startGame(gameId)
        assert gameDao.findGameById(gameId).status == "ACTIVE"
        assert gameDao.findGameById(gameId).pending == null
    }

    @Test
    void testWinGame() {
        def game = [gameName: "fooGame3", numPlayers: 3, rounds: 1, isOpen: false, teamSize: 1, status: "ACTIVE"]
        def winners = ["p1", "p5"]
        gameDao.createGame(game)
        def gameId = game._id
        gameDao.winGame(gameId, winners)
        assert gameDao.findGameById(gameId).status == "FINISHED"
        assert gameDao.findGameById(gameId).winners == ["p1", "p5"]
    }

    @Test
    void testQuitGame() {
        def game = [gameName: "fooGame4", playing: ["p1", "p2", "p3"], rounds: 1, isOpen: false, teamSize: 1, status: "ACTIVE"]
        gameDao.createGame(game)
        def gameId = game._id
        gameDao.quitGame(gameId, "p3")
        assert gameDao.findGameById(gameId).playing == ["p1", "p2"]
    }

    @Test
    void testInvite() {
        def game = [gameName: "fooGame5", pending: ["p1", "p2", "p3"], rounds: 1, isOpen: false, teamSize: 1, status: "PENDING"]
        gameDao.createGame(game)
        def gameId = game._id
        gameDao.invite(gameId, "p4")
        def result = gameDao.findGameById(gameId)
        assert result["pending"] == ["p1", "p2", "p3", "p4"]
    }

    @Test
    void testUnInvite() {
        def game = [gameName: "fooGame6", pending: ["p1", "p2", "p3"], rounds: 1, isOpen: false, teamSize: 1, status: "PENDING"]
        gameDao.createGame(game)
        def gameId = game._id
        gameDao.uninvite(gameId, "p3")
        def result = gameDao.findGameById(gameId)
        assert result["pending"] == ["p1", "p2"]
    }

    @Test
    void testDeclineInvite() {
        def game = [gameName: "fooGame7", pending: ["p1", "p2", "p3"], rounds: 1, isOpen: false, teamSize: 1, status: "PENDING"]
        gameDao.createGame(game)
        def gameId = game._id
        gameDao.uninvite(gameId, "p3")
        def result = gameDao.findGameById(gameId)
        assert result["pending"] == ["p1", "p2"]
    }

    @Test
    void testStartNextRound() {
        def game = [gameName: "fooGame8", playing: ["p1", "p2", "p3"], currentRound: 1, rounds: 2, isOpen: false, teamSize: 1, status: "ACTIVE"]
        gameDao.createGame(game)
        def gameId = game._id
        def categories = ["actor", "game", "color"]
        gameDao.startNextRound(gameId, "L", categories)
        def result = gameDao.findGameById(gameId)
        assert result["currentRound"] == 2
    }

    @Test
    void testJoinGame() {
        def game = [gameName: "fooGame9", playing: ["p1", "p2", "p3"], pending: ["p4"], currentRound: 1, rounds: 2, teamSize: 1, status: "PENDING"]
        gameDao.createGame(game)
        def gameId = game._id
        def result = gameDao.joinGame(gameId, "p4")
        assert result["playing"] == ["p1", "p2", "p3", "p4"]
        assert result["pending"] == []
    }

    @Test
    void testRemovePlayer() {
        def game = [gameName: "barGame", playing: ["p1", "p2", "p3"], pending: ["p4"], currentRound: 1, rounds: 2, teamSize: 1, status: "PENDING"]
        gameDao.createGame(game)
        def gameId = game._id
        gameDao.removePlayer(gameId, "p4")
        def result = gameDao.findGameById(gameId)
        assert result["playing"] == ["p1", "p2", "p3"]
        assert result["pending"] == []
        gameDao.removePlayer(gameId, "p2")
        result = gameDao.findGameById(gameId)
        assert result["playing"] == ["p1", "p3"]
        assert result["pending"] == []
    }

    @Test
    void testFindUserGamesByStatus() {
        def game = [gameName: "barGame2", playing: ["p1", "p2", "p3"], pending: ["p4"], currentRound: 1, rounds: 2, teamSize: 1, status: "PENDING"]
        def game2 = [gameName: "bazGame", playing: ["p1", "p2", "p3"], pending: ["p4"], currentRound: 1, rounds: 2, teamSize: 1, status: "PENDING"]
        def game3 = [gameName: "bazGame2", playing: ["p1", "p2", "p3"], currentRound: 1, rounds: 2, teamSize: 1, status: "ACTIVE"]
        gameDao.createGame(game)
        gameDao.createGame(game2)
        gameDao.createGame(game3)
        def games = gameDao.findUserGamesByStatus("p2", "PENDING")
        assert games.size() == 2
        assert games["gameName"].containsAll(["barGame2", "bazGame"])
    }

    @Test
    void testGetWinners() {
        def wonGame = [gameName: "wonGame", playing: ["p1", "p2", "p3"], currentRound: 1, rounds: 2, teamSize: 1, status: "ACTIVE"]
        gameDao.createGame(wonGame)
        def gameId = wonGame._id
        assert gameDao.getWinners(gameId) == null
        gameDao.winGame(gameId, ["p1", "p2"])
        assert gameDao.getWinners(gameId) == ["p1", "p2"]
    }

    @Test
    void testFindInvitedGamesForPlayer() {
        def game = [gameName: "fooGame5", pending: ["p1", "p2", "p3"], rounds: 1, isOpen: false, teamSize: 1, status: "PENDING"]
        gameDao.createGame(game)
        def gameId = game._id
        def result = gameDao.findInvitedGames("p3")
        assert result["gameName"].size == 1
        assert result["gameName"].get(0) == "fooGame5"
    }

    private static class MockGameDAO extends GameDao {
        private final static DB_NAME = "scatterbrained-test"
        def mockDB

        @Override
        protected DB getMongoDB() {
            if (mockDB == null) {
                gmongo = new GMongo(new DBAddress('localhost', 27017, DB_NAME))
                gmongo.setWriteConcern(WriteConcern.SAFE)
                mockDB = gmongo.getDB(DB_NAME)
            }
            mockDB
        }
    }

}
