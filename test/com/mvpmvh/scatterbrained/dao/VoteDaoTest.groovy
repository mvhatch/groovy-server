package com.mvpmvh.scatterbrained.dao

import com.gmongo.GMongo
import com.mongodb.DB
import com.mongodb.DBAddress
import com.mongodb.WriteConcern
import org.bson.types.ObjectId
import org.junit.After
import org.junit.AfterClass
import org.junit.Test

class VoteDaoTest {
    static MockVoteDao voteDao = new MockVoteDao()

    @AfterClass
    static void tearDownAfterClass() {
        voteDao.getMongoDB().dropDatabase()
    }

    @After
    void tearDown() {
        voteDao.getMongoDB().votes.drop()
    }

    @Test
    void testVoteOnPlayer() {
        def id = new ObjectId()
        assert voteDao.getPlayerVotesForGameByRound(id, "fooGame", 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barPlayer", true)
        assert voteDao.getPlayerVotesForGameByRound("fooVoter", id, 1).size() == 2
    }

    @Test
    void testGetAllPlayerVotesForGame() {
        def id = new ObjectId()
        assert voteDao.getPlayerVotesForGameByRound("fooVoter", id, 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", true)
        assert voteDao.getAllPlayerVotesForGame("fooVoter", id).size() == 3
        assert voteDao.getAllPlayerVotesForGame("barVoter", id).size() == 1
    }

    @Test
    void testQuitGame() {
        def id = new ObjectId()
        assert voteDao.getPlayerVotesForGameByRound("fooVoter", id, 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", true)
        assert voteDao.getAllPlayerVotesForGame("fooVoter", id).size() == 3
        assert voteDao.getAllPlayerVotesForGame("barVoter", id).size() == 1
        voteDao.quitGame(id, "fooPlayer")
        assert voteDao.getAllPlayerVotesForGame("fooVoter", id).size() == 2
        assert voteDao.getAllPlayerVotesForGame("barVoter", id).size() == 0
    }

    @Test
    void testVotesOnPlayerByRound() {
        def id = new ObjectId()
        assert voteDao.getVotesOnPlayerByRound("fooPlayer", id, 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", true)
        assert voteDao.getVotesOnPlayerByRound("fooPlayer", id, 1).size() == 2
        assert voteDao.getVotesOnPlayerByRound("barPlayer", id, 1).size() == 1
        voteDao.quitGame(id, "fooPlayer")
        assert voteDao.getAllVotesOnPlayerForGame("fooPlayer", id).size() == 0
        assert voteDao.getAllVotesOnPlayerForGame("barPlayer", id).size() == 2
    }

    @Test
    void testGetPlayerUnfinishedVotingListForRound() {
        def id = new ObjectId()
        assert voteDao.getPlayerUnfinishedVotingListForRound(id, "fooVoter", 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", false)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", true)
        assert voteDao.getPlayerUnfinishedVotingListForRound(id, "fooVoter", 1).size() == 1
    }

    @Test
    void testGetPlayerFinishedVotingListForRound() {
        def id = new ObjectId()
        assert voteDao.getPlayerFinishedVotingListForRound(id, "fooVoter", 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", false)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", true)
        assert voteDao.getPlayerFinishedVotingListForRound(id, "fooVoter", 1).size() == 1
    }

    @Test
    void testGetPlayerTotalVotingListForRound() {
        def id = new ObjectId()
        assert voteDao.getPlayerTotalVotingListForRound(id, "fooVoter", 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", false)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", true)
        assert voteDao.getPlayerTotalVotingListForRound(id, "fooVoter", 1).size() == 2
    }

    @Test
    void testGetWhoCanVote() {
        def id = new ObjectId()
        assert voteDao.getWhoCanVote(id, 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", false)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", true)
        assert voteDao.getWhoCanVote(id, 1).size() == 2
    }

    @Test
    void testAddNewVoter() {
        def id = new ObjectId()
        assert voteDao.getWhoCanVote(id, 1) == []
        voteDao.addNewVoter(id, 1, "fooPlayer")
        voteDao.addNewVoter(id, 1, "barPlayer")
        voteDao.addNewVoter(id, 2, "bazPlayer")
        assert voteDao.getWhoCanVote(id, 1).size() == 2
    }

    @Test
    void testPrepareToVoteOn() {
        def id = new ObjectId()
        assert voteDao.getAllVotesOnPlayerForGame("barPlayer", id) == []
        assert voteDao.getAllPlayerVotesForGame("fooVoter", id) == []
        voteDao.prepareToVoteOn("barPlayer", id, 1, "fooVoter")
        assert voteDao.getAllVotesOnPlayerForGame("barPlayer", id).size() == 1
        assert voteDao.getAllPlayerVotesForGame("fooVoter", id).size() == 1
    }

    @Test
    void testGetUnfinishedVotersForRound() {
        def id = new ObjectId()
        assert voteDao.getUnfinishedVotersForRound(id, 1) == []
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("barPlayer", id, 1, [true, false], "fooVoter", false)
        voteDao.voteOnPlayer("barPlayer", id, 2, [true, false], "fooVoter", true)
        voteDao.voteOnPlayer("fooPlayer", id, 1, [true, false], "barVoter", false)
        assert voteDao.getUnfinishedVotersForRound(id, 1).size() == 2
    }



    private static class MockVoteDao extends VoteDao {
        private final static DB_NAME = "scatterbrained-test"
        def mockDB

        @Override
        protected DB getMongoDB() {
            if (mockDB == null) {
                gmongo = new GMongo(new DBAddress('localhost', 27017, DB_NAME))
                gmongo.setWriteConcern(WriteConcern.SAFE)
                mockDB = gmongo.getDB(DB_NAME)
            }
            mockDB
        }
    }

}
