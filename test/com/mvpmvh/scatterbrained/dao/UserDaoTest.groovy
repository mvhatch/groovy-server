package com.mvpmvh.scatterbrained.dao

import com.gmongo.GMongo
import com.mongodb.DB
import com.mongodb.DBAddress
import com.mongodb.WriteConcern
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.Test

class UserDaoTest {
    static MockUserDAO userDAO = new MockUserDAO()

    @Before
    void setUp() {
        //userDAO = new MockUserDAO()
    }

    @AfterClass
    static void tearDownAfterClass() {
        userDAO.getMongoDB().dropDatabase()
    }

    @After
    void tearDown() {
        userDAO.getMongoDB().users.drop()
    }

    @Test
    void testAddandFindUser() {
        def newUser = [username: "userFoo", password: "fooPass", email: "foo@foo", buddies: []]
        assert userDAO.findUser(newUser) == null
        userDAO.addUser(newUser)
        assert userDAO.findUser("userFoo") != null
    }
    /* TODO: update test for BCrypt usage
    @Test
    void testFindUserandPassword() {
        def newUser = [username: "userFoo2", password: "fooPass", email: "foo@foo", buddies: []]
        assert userDAO.findUser(newUser) == null
        userDAO.addUser(newUser)
        assert userDAO.findUser("userFoo2", "fooPass") != null
    }*/

    @Test
    void testUpdateEmail() {
        def newUser = [username: "userFoo3", password: "fooPass", email: "foo@foo", buddies: []]
        assert userDAO.findUser(newUser) == null
        userDAO.addUser(newUser)
        assert userDAO.findUser("userFoo3", "fooPass").email == "foo@foo"
        userDAO.updateUserEmail("userFoo3", "bar@bar")
        assert userDAO.findUser("userFoo3", "fooPass").email == "bar@bar"
    }

    /* TODO: update test for BCrypt usage
    @Test
    void testUpdatePassword() {
        def newUser = [username: "userFoo4", password: "fooPass", email: "foo@foo", buddies: []]
        assert userDAO.findUser(newUser) == null
        userDAO.addUser(newUser)
        assert userDAO.findUser("userFoo4").password == "fooPass"
        userDAO.updatePassword("userFoo4", "randomPassword")
        assert userDAO.findUser("userFoo4").password == "randomPassword"
    }*/

    @Test
    void testAddBuddy() {
        def newUser = [username: "userFoo5", password: "fooPass", email: "foo@foo", buddies: []]
        assert userDAO.findUser(newUser) == null
        userDAO.addUser(newUser)
        def buddies = userDAO.findUser("userFoo5").buddies
        assert buddies.size() == 0
        userDAO.addBuddy("userFoo5", "userBar")
        buddies = userDAO.findUser("userFoo5").buddies
        assert buddies.size() == 1
    }

    @Test
    void testRemoveBuddy() {
        def newUser = [username: "userFoo6", password: "fooPass", email: "foo@foo", buddies: ["bazUser"]]
        assert userDAO.findUser(newUser) == null
        userDAO.addUser(newUser)
        def buddies = userDAO.findUser("userFoo6").buddies
        assert buddies.size() == 1
        userDAO.removeBuddy("userFoo6", "nonExistentUser")
        buddies = userDAO.findUser("userFoo6").buddies
        assert buddies.size() == 1
        userDAO.removeBuddy("userFoo6", "bazUser")
        buddies = userDAO.findUser("userFoo6").buddies
        assert buddies.size() == 0
    }

    @Test
    void testFindEmail() {
        def newUser = [username: "userFoo7", password: "fooPass", email: "foo@foo", buddies: []]
        assert userDAO.findUser(newUser) == null
        assert userDAO.findEmail("userFoo7") == null
        userDAO.addUser(newUser)
        assert userDAO.findEmail("userFoo7") == "foo@foo"
    }

    @Test
    void testFindUserByEmail() {
        def newUser = [username: "userFoo8", password: "fooPass", email: "foo8@foo", buddies: []]
        assert userDAO.findUserByEmail("foo8@foo") == null
        userDAO.addUser(newUser)
        assert userDAO.findUserByEmail("foo8@foo") == "userFoo8"
    }


    private static class MockUserDAO extends UserDao {
        private final static DB_NAME = "scatterbrained-test"
        def mockDB

        @Override
        protected DB getMongoDB() {
            if (mockDB == null) {
                gmongo = new GMongo(new DBAddress('localhost', 27017, DB_NAME))
                gmongo.setWriteConcern(WriteConcern.SAFE)
                mockDB = gmongo.getDB(DB_NAME)
            }
            mockDB
        }
    }


}
