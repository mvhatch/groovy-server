package com.mvpmvh.scatterbrained.dao

import com.gmongo.GMongo
import com.mongodb.DB
import com.mongodb.DBAddress
import com.mongodb.WriteConcern
import org.bson.types.ObjectId
import org.junit.After
import org.junit.AfterClass
import org.junit.Test

class AnswerDaoTest {
    static MockAnswerDao answerDao = new MockAnswerDao()

    @AfterClass
    static void tearDownAfterClass() {
        answerDao.getMongoDB().dropDatabase()
    }

    @After
    void tearDown() {
        answerDao.getMongoDB().answers.drop()
    }

    @Test
    void saveAnswersForPlayerForRound() {
        def _answers = ["a", "b", "c", "d"]
        def id = new ObjectId()
        assert answerDao.getAnswersForPlayerByRound(id, "fooPlayer", 1) == null
        answerDao.saveAnswersForPlayerForRound(id, "fooPlayer", 1, _answers, false)
        assert answerDao.getAnswersForPlayerByRound(id, "fooPlayer", 1) != null
    }

    @Test
    void testQuitGame() {
        def _answers = ["a", "b", "c", "d"]
        def id = new ObjectId()
        assert answerDao.getAnswersForPlayerByRound(id, "fooPlayer", 1) == null
        answerDao.saveAnswersForPlayerForRound(id, "fooPlayer", 1, _answers, true)
        answerDao.saveAnswersForPlayerForRound(id, "fooPlayer", 2, _answers, true)
        answerDao.saveAnswersForPlayerForRound(id, "fooPlayer", 3, _answers, false)
        answerDao.quitGame(id, "fooPlayer")
        assert answerDao.getAnswersForPlayerByRound(id, "fooPlayer", 1) == null
        assert answerDao.getAnswersForPlayerByRound(id, "fooPlayer", 2) == null
        assert answerDao.getAnswersForPlayerByRound(id, "fooPlayer", 3) == null
    }

    @Test
    void testGetAllUnfinishedAnswers() {
        def _answers = ["a", "b", "c", "d"]
        def id = new ObjectId()
        assert answerDao.getAnswersForPlayerByRound(id, "fooPlayer1", 1) == null
        answerDao.saveAnswersForPlayerForRound(id, "fooPlayer1", 1, _answers, true)
        answerDao.saveAnswersForPlayerForRound(id, "fooPlayer2", 1, _answers, true)
        answerDao.saveAnswersForPlayerForRound(id, "fooPlayer3", 1, _answers, false)
        def unfinished = answerDao.getAllUnfinishedAnswersForRound(id, 1)
        assert unfinished.size() == 1
        def unfinishedAnswer = unfinished.get(0)
        assert unfinishedAnswer["username"] == "fooPlayer3"
    }

    private static class MockAnswerDao extends AnswerDao {
        private final static DB_NAME = "scatterbrained-test"
        def mockDB

        @Override
        protected DB getMongoDB() {
            if (mockDB == null) {
                gmongo = new GMongo(new DBAddress('localhost', 27017, DB_NAME))
                gmongo.setWriteConcern(WriteConcern.SAFE)
                mockDB = gmongo.getDB(DB_NAME)
            }
            mockDB
        }
    }
}
